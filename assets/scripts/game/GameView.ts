import { _decorator, EventTouch, math, Node, Sprite, UITransform, v3, Vec2 } from "cc";
import { BUILD } from "cc/env";
import { PresenterView, ResourceUtils, ViewType } from "../frame";
import { GameActionMgr } from "./GameActionMgr";
import { GameLatticeComp } from "./GameLatticeComp";
import { GameModel } from "./GameModel";
import { GamePresenter } from "./GamePresenter";
import { GMgr, MessageType } from "./GMgr";
import { GSound } from "./GSound";

const { ccclass, property } = _decorator;


@ccclass('GameView')
export class GameView extends PresenterView {
    public static url: string = 'game/gameView';
    public static viewType: ViewType = ViewType.Page;

    public presenter: GamePresenter;
    private main: Node;
    private actionMgr: GameActionMgr;
    private _latticeSize: game.IPos;
    private _moveLatticeList: Array<GameLatticeComp> = [];
    private _isTouch: boolean = false;
    private _latticeWidth: number = 0;
    public onEnable() {
        super.onEnable();
        if (BUILD) {
            //延时一帧执行，保证界面已经完全显示出来
            this.scheduleOnce(() => {
                //加载成功，执行app的加载成功
                GameModel.instance().runNativeFunc('resLoadSuccess');
            }, 0)
        }
        this.node.on(Node.EventType.TOUCH_START, this.touchStart, this);
        this.node.on(Node.EventType.TOUCH_MOVE, this.touchMove, this);
        this.node.on(Node.EventType.TOUCH_END, this.touchEnd, this);
        this.node.on(Node.EventType.TOUCH_CANCEL, this.touchCancel, this);
        GSound.playerMusic();
    }
    protected onLoad(): void {
        this.createMap();
    }
    private touchStart(event: EventTouch) {
        if (!this._isTouch) return
        let location = event.getUILocation();
        this.addLatticeNode(location);
    }
    private touchMove(event: EventTouch) {
        if (!this._isTouch) return
        let location = event.getUILocation();
        this.addLatticeNode(location);
    }
    private touchCancel(event: EventTouch) {
        if (!this._isTouch) return
        this.onSelectEnd();
    }
    private touchEnd(event: EventTouch) {
        if (!this._isTouch) return
        this.onSelectEnd();
    }
    /**选择操作结束 */
    private onSelectEnd() {
        if (this._moveLatticeList.length == 0) return;
        this._isTouch = false;
        this.actionMgr.runTrailingAnimation(this._moveLatticeList, () => {
            this._isTouch = true;
            // this.presenter.startGame();
        });
    }
    /**
     * 通过当前触控点获取一个格子节点
     * @param pos 坐标点
     */
    private addLatticeNode(pos: Vec2) {
        const startNode = this.main.children[0];
        let lPos: game.IPos = { x: 0, y: 0 }
        lPos.x = Math.floor((pos.x - (startNode.position.x - this._latticeWidth / 2)) / this._latticeWidth);
        if (lPos.x < 0) lPos.x = 0;
        if (lPos.x >= 12) lPos.x = 11;
        lPos.y = Math.floor((pos.y - (startNode.position.y - this._latticeWidth / 2)) / this._latticeWidth);
        if (lPos.y < 0) lPos.y = 0;
        if (lPos.y >= this._latticeSize.y) lPos.y = this._latticeSize.y - 1;

        let lattice: GameLatticeComp = this.actionMgr.getLatticeByPos(lPos);
        if (!lattice) return;
        let backLattice = this._moveLatticeList[this._moveLatticeList.length - 1];
        if (lattice == backLattice) return;
        this.repairLatticeList(lattice, backLattice);
        this._moveLatticeList.push(lattice);
        this.actionMgr.setTrailingColor(this._moveLatticeList);
        GSound.playEffect(lattice.getSoundId());
        //这里传给玩具值
        GMgr.instance().runNativeFunc('updateGame', JSON.stringify({
            x: lattice.pos.x / (this._latticeSize.x - 1),
            y: lattice.pos.y / (this._latticeSize.y - 1),
            isDraging: true,
        }));
    }
    /**修正格子数组 */
    private repairLatticeList(lattice: GameLatticeComp, backLattice: GameLatticeComp) {
        if (!lattice || !backLattice) return;
        let cx = lattice.pos.x - backLattice.pos.x;
        let cy = lattice.pos.y - backLattice.pos.y;
        if (Math.abs(cx) <= 1 && Math.abs(cy) <= 1) return;
        let lPos: game.IPos;
        if (Math.abs(cx) == Math.abs(cy)) {
            lPos = {
                x: backLattice.pos.x + Math.abs(cx) / cx,
                y: backLattice.pos.y + Math.abs(cy) / cy
            }
        } else if (Math.abs(cx) > Math.abs(cy)) {
            lPos = {
                x: backLattice.pos.x + Math.abs(cx) / cx,
                y: backLattice.pos.y
            }
        } else if (Math.abs(cx) < Math.abs(cy)) {
            lPos = {
                x: backLattice.pos.x,
                y: backLattice.pos.y + Math.abs(cy) / cy
            }
        }
        if (!lPos) return;
        let backLattice2: GameLatticeComp = this.actionMgr.getLatticeByPos(lPos);
        this._moveLatticeList.push(backLattice2);
        this.repairLatticeList(lattice, backLattice2);
    }
    /**创建全部格子 */
    private createMap() {
        if (this.actionMgr) return;
        const transform = this.node.getComponent(UITransform)
        this._latticeWidth = transform.width / 12;
        let nWidth = this._latticeWidth - 5;
        let hCount = Math.floor(transform.height / this._latticeWidth);
        if (hCount < 20) {
            hCount = 20;
            this._latticeWidth = transform.height / hCount;
            nWidth = this._latticeWidth - 5;
        }
        const xh = (transform.height - this._latticeWidth * hCount) / 2;
        const wCount = 12;
        for (let i = 0; i < hCount; i++) {
            for (let j = 0; j < wCount; j++) {
                let node = this.createLattice(math.size(nWidth, nWidth), { x: j, y: i });
                node.position = v3((j + 0.5) * this._latticeWidth, (i + 0.5) * this._latticeWidth + xh);
                this.main.addChild(node);
            }
        }
        this._latticeSize = {
            x: 12,
            y: hCount
        }
        this.actionMgr = new GameActionMgr(this.main.children, this._latticeSize);
    }
    /**获取一个格子 */
    private createLattice(contentSize: math.Size, pos: game.IPos) {
        let node = new Node('lattice');
        let transform = node.addComponent(UITransform);
        transform.contentSize = contentSize;
        let sprite = node.addComponent(Sprite);
        sprite.sizeMode = Sprite.SizeMode.CUSTOM;
        sprite.spriteFrame = ResourceUtils.getSpriteframe('img/main/img_3', '');
        let latticeComp = node.addComponent(GameLatticeComp);
        latticeComp.setColor(GameLatticeComp.imgColor.none);
        latticeComp.pos.x = pos.x;
        latticeComp.pos.y = pos.y;
        return node;
    }
    public startGame() {
        this.actionMgr.runStartGameTime(() => {
            GMgr.instance().sendMessage({
                type: MessageType.GAME_START
            })
            this._isTouch = true;
        });
    }
}