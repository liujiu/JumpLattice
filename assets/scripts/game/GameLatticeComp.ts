import { Component, Node, Sprite, UITransform, _decorator, math } from "cc";
import { ResourceUtils } from "../frame";

const { ccclass, property } = _decorator;

const ImgColor = {
    none: 0,
    red: 7,//红
    yellow: 1,//黄
    blue2: 4,//蓝
    blue: 8,//蓝
    green: 2,//绿
    orange: 6,//橙
    purple: 5,//紫
}
/**
 * 格子组件
 */
@ccclass('GameLatticeComp')
export class GameLatticeComp extends Component {
    public static imgColor = ImgColor;
    private color: Node;
    public imgColor: number;
    public pos: game.IPos = { x: 0, y: 0 };
    public onEnable() {
        this.createColorNode();
        this.setColor(this.imgColor);
    }
    private createColorNode() {
        if (this.color) return;
        const mTransform = this.node.getComponent(UITransform);
        const width = 210 / 150 * mTransform.width;
        let node = this.color = new Node('color');
        let transform = node.addComponent(UITransform);
        node.addComponent(Sprite).sizeMode = Sprite.SizeMode.CUSTOM;
        transform.contentSize = math.size(width, width);
        node.active = false;
        this.node.addChild(node);
    }
    public setColor(imgColor: number, alpha?: number) {
        if (!imgColor) imgColor = ImgColor.none;
        this.imgColor = imgColor;
        if (!this.color) return;
        if (imgColor == ImgColor.none) {
            this.color.active = false;
        } else {
            if (typeof alpha == 'number') {
                this.setAlpha(alpha);
            } else {
                this.setAlpha(255);
            }
            this.color.active = true;
            this.color.getComponent(Sprite).spriteFrame = ResourceUtils.getSpriteframe('img/main/img_' + imgColor, '');
        }
    }
    public setAlpha(alpha: number) {
        this.color.getComponent(Sprite).color = new math.Color(255, 255, 255, alpha);
    }
    public getSoundId(): string {
        if (this.imgColor == ImgColor.orange) {
            return 'bt1';
        } else if (this.imgColor == ImgColor.purple) {
            return 'bt2';
        } else if (this.imgColor == ImgColor.blue2) {
            return 'bt1';
        }
        return 'bt1';
    }
}