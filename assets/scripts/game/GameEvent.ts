import { Event } from "../frame";

export class GameEvent extends Event {
    /**开始游戏 */
    public static START_GAME = 'GameEvent.START_GAME';
    /**跳跃 */
    public static JUMP_ACTION = 'GameEvent.JUMP_ACTION';
}