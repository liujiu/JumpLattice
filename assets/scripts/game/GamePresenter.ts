import { _decorator } from "cc";
import { BasePresenter } from "../frame";
import { GameEvent } from "./GameEvent";
import { GameModel } from "./GameModel";
import { GameView } from "./GameView";

const { ccclass } = _decorator;

@ccclass('GamePresenter')
export class GamePresenter extends BasePresenter {
    protected getViewClass() {
        return GameView;
    }
    protected getModelClass() {
        return GameModel;
    }
    public view: GameView;
    public model: GameModel;

    protected bindEvents(): void {
        GameEvent.onList(this, [
            [GameEvent.START_GAME, this.startGame]
        ])
    }
    protected removeEvents(): void {
        GameEvent.targetOff(this);
    }
    protected viewShowEnd(): void {
        this.startGame();
    }
    /**开始游戏 */
    public startGame() {
        this.model.reset();
        this.view.startGame();
    }
}