import { SoundMgr } from "../frame";


export class GSound {
    private static effectDate = 0;
    private static isVoice: boolean = true;
    public static openVoice() {
        this.isVoice = true;
        this.playerMusic();
    }
    public static closeVoice() {
        this.isVoice = false;
        SoundMgr.instance().closeEffect();
        SoundMgr.instance().closeMusic();
    }
    public static playEffect(id: string = 'bt3') {
        if (!this.isVoice) return;
        // if (Date.now() - this.effectDate < 20) return;
        this.effectDate = Date.now();
        SoundMgr.instance().closeEffect();
        SoundMgr.instance().playEffect({
            path: id
        })
    }
    public static playerMusic() {
        if (!this.isVoice) return;
        SoundMgr.instance().playMusic({
            path: 'bg'
        })
    }
}