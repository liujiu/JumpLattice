import { _decorator, sys } from 'cc';
import { BaseModel, Log } from '../frame';

const { ccclass } = _decorator

@ccclass('GameModel')
export class GameModel extends BaseModel {
    protected localData = {
        /**引导 */
        isGuide: false
    };
    /**引导 */
    public set guide(value: boolean) {
        this.localData.isGuide = value;
    }
    public get guide(): boolean {
        return this.localData.isGuide;
    }

    public reset() {
    }
    /**
     * 
     * @param funcName 
     * @param jsonStr 
     * @returns 
     */
    public runNativeFunc(funcName: string, jsonStr?: string): boolean {
        Log.log(funcName, jsonStr);
        if (sys.os == sys.OS.ANDROID || sys.os == sys.OS.LINUX) {
            if (window['Android'] && window['Android'][funcName]) {
                window['Android'][funcName](jsonStr);
                return true;
            }
        } else if (sys.os == sys.OS.IOS) {
            if (window['webkit'] && window['webkit'].messageHandlers && window['webkit'].messageHandlers[funcName]) {
                window['webkit'].messageHandlers[funcName].postMessage(jsonStr);
                return true
            }
        }
        return false
    }
}