import { Node, tween } from "cc";
import { GMgr } from "./GMgr";
import { GSound } from "./GSound";
import { GameCfg } from "./GameCfg";
import { GameLatticeComp } from "./GameLatticeComp";

/**动画管理器 */
export class GameActionMgr {
    private _latticeList: Array<GameLatticeComp> = []
    private _latticeSize: game.IPos;
    private _merryGoRound: Array<{ x: string, y: string }>;
    private _merryGoRoundIntervalId: number;
    private _trailingIntervalId: number;
    constructor(nodes: Array<Node>, latticeSize: game.IPos) {
        nodes.forEach(node => {
            this._latticeList.push(node.getComponent(GameLatticeComp));
        })
        this._latticeSize = latticeSize
        this._merryGoRound = [
            { x: '0', y: `${this._latticeSize.y - 1}-0` },
            { x: '0-11', y: `0` },
            { x: '11', y: `0-${this._latticeSize.y - 1}` },
            { x: '11-0', y: `${this._latticeSize.y - 1}` },

            { x: '1', y: `${this._latticeSize.y - 2}-0` },
            { x: '1-10', y: `1` },
            { x: '10', y: `1-${this._latticeSize.y - 2}` },
            { x: '10-1', y: `${this._latticeSize.y - 2}` }
        ]
    }
    /**
     * 获取格子，这里的坐标是格子坐标系的坐标
     */
    public getLatticeByPos(pos: game.IPos): GameLatticeComp {
        return this._latticeList[pos.x + pos.y * 12]
    }
    /**执行拖尾动画 */
    public runTrailingAnimation(array: Array<GameLatticeComp>, callback: Function) {
        clearInterval(this._trailingIntervalId);
        const len = array.length;
        let color = GameLatticeComp.imgColor.orange;
        let comp1 = array[len - 1];
        let comp2 = array[len - 1];
        if (comp1 && comp2) {
            if (comp1.pos.x == comp2.pos.x) color = GameLatticeComp.imgColor.blue2;
            if (comp1.pos.y == comp2.pos.y) color = GameLatticeComp.imgColor.purple;
        }
        let endComp = array[array.length - 1];
        //这里传给玩具值
        GMgr.instance().runNativeFunc('updateGame', JSON.stringify({
            x: endComp.pos.x / (this._latticeSize.x - 1),
            y: endComp.pos.y / (this._latticeSize.y - 1),
            isDraging: false,
            duration: array.length * 20,
        }));
        this._trailingIntervalId = setInterval(() => {
            let lattice = array[0];
            GSound.playEffect(lattice.getSoundId());
            array.splice(0, 1);
            if (array.indexOf(lattice) == -1)
                lattice.setColor(GameLatticeComp.imgColor.none);
            if (array.length == 0) {
                clearInterval(this._trailingIntervalId);
                callback();
            }
        }, 20)
    }
    /**重置拖尾的透明度 */
    public setTrailingColor(array: Array<GameLatticeComp>, color?: number) {
        const len = array.length;
        for (let i = 0; i < len; i++) {
            let lattice = array[i];
            let lattice2 = array[i - 1] || array[i + 1];
            if (lattice2) {
                color = GameLatticeComp.imgColor.orange;
                if (lattice.pos.x == lattice2.pos.x) color = GameLatticeComp.imgColor.purple;
                if (lattice.pos.y == lattice2.pos.y) color = GameLatticeComp.imgColor.blue2;
            } else {
                color = lattice.imgColor == GameLatticeComp.imgColor.none ? GameLatticeComp.imgColor.purple : lattice.imgColor;
            }
            if (len > 12) {
                lattice.setColor(color, 255 - (len - 1 - i) * 245 / len);
            } else {
                lattice.setColor(color, 255 - (len - 1 - i) * 245 / 12);
            }
        }
    }
    /**
     * 播放走马灯
     * x:0,y:max-0
     * x:0-12,y:0
     * x:12,y:0-max
     * x:12-0,y:max
     * 
     * x:1,y:(max-1)-0
     * x:1-11,y:1
     * x:11,y:1-(max-1)
     * x:11-1,y:(max-1)
     */
    public runMerryGoRound(callback: Function) {
        this.setAllColor('none');
        clearInterval(this._merryGoRoundIntervalId);
        let list: Array<GameLatticeComp> = [];
        let idx = 0;
        do {
            const cfg = this._merryGoRound[idx];
            let xList = cfg.x.split('-').map(Number);
            let yList = cfg.y.split('-').map(Number);
            if (xList.length == 2) {
                for (let i = 0; i < Math.abs(xList[0] - xList[1]); i++) {
                    list.push(this.getLatticeByPos({
                        x: xList[0] < xList[1] ? xList[0] + i : xList[0] - i,
                        y: yList[0]
                    }))
                }
            } else if (yList.length == 2) {
                for (let i = 0; i < Math.abs(yList[0] - yList[1]); i++) {
                    list.push(this.getLatticeByPos({
                        x: xList[0],
                        y: yList[0] < yList[1] ? yList[0] + i : yList[0] - i
                    }))
                }
            }
        } while (++idx < this._merryGoRound.length);
        idx = 0;

        let setColor = function (comp: GameLatticeComp) {
            if (!comp) return;
            comp.setColor(GameLatticeComp.imgColor.green);
        }
        let runList: Array<Array<GameLatticeComp>> = [];
        for (let i = this._latticeSize.y - 1; i >= 0; i--) {
            let itemList = [];
            itemList.push(this.getLatticeByPos({
                x: 0,
                y: i
            }))
            itemList.push(this.getLatticeByPos({
                x: 1,
                y: i
            }))
            runList.push(itemList);
        }
        for (let i = 2; i < this._latticeSize.x - 2; i++) {
            let itemList = [];
            itemList.push(this.getLatticeByPos({
                x: i,
                y: 0
            }))
            itemList.push(this.getLatticeByPos({
                x: i,
                y: 1
            }))
            runList.push(itemList);
        }
        for (let i = 0; i < this._latticeSize.y; i++) {
            let itemList = [];
            itemList.push(this.getLatticeByPos({
                x: 10,
                y: i
            }))
            itemList.push(this.getLatticeByPos({
                x: 11,
                y: i
            }))
            runList.push(itemList);
        }
        for (let i = this._latticeSize.x - 3; i >= 2; i--) {
            let itemList = [];
            itemList.push(this.getLatticeByPos({
                x: i,
                y: this._latticeSize.y - 1
            }))
            itemList.push(this.getLatticeByPos({
                x: i,
                y: this._latticeSize.y - 2
            }))
            runList.push(itemList);
        }

        this._merryGoRoundIntervalId = setInterval(() => {
            runList[idx] && setColor(runList[idx][0]);
            runList[idx] && setColor(runList[idx][1]);
            runList[idx + 1] && setColor(runList[idx + 1][0]);
            runList[idx + 1] && setColor(runList[idx + 1][1]);
            idx += 2;
            if (idx >= runList.length) {
                clearInterval(this._merryGoRoundIntervalId);
                callback();
            }
        }, 30)
    }
    /**
     * 开始游戏倒计时
     * @param callback 动画播放完成回调
     */
    public runStartGameTime(callback: Function) {
        this.runMerryGoRound(() => {
            this.showNumCfg(GameCfg.num['K'] as game.INumCfgInfo);
            this.runKRed().then(() => {
                tween(this)
                    .call(() => {
                        this.setAllColor('yellow');
                    }).delay(1)
                    .call(() => {
                        this.setAllColor('blue');
                    }).delay(1)
                    .call(() => {
                        this.showNumCfg(GameCfg.num['3'] as game.INumCfgInfo);
                        GSound.playEffect('321');
                    }).delay(1)
                    .call(() => {
                        this.showNumCfg(GameCfg.num['2'] as game.INumCfgInfo);
                    }).delay(1)
                    .call(() => {
                        this.showNumCfg(GameCfg.num['1'] as game.INumCfgInfo);
                    }).delay(1)
                    .call(() => {
                        this.setAllColor('none')
                        callback();
                    })
                    .start();
            })
        })
    }
    /**播放K的红色跑马动画 */
    private runKRed(): Promise<undefined> {
        return new Promise((resolve, reject) => {
            let runList: Array<Array<GameLatticeComp>> = [];
            for (let y = this._latticeSize.y - 2; y >= 1; y--) {
                let itemList = [];
                for (let x = 2; x < this._latticeSize.x - 2; x++) {
                    itemList.push(this.getLatticeByPos({ x: x, y: y }))
                }
                runList.push(itemList);
            }
            let idx = 0;
            let count = 0;
            let setItemListColor = function (compList: Array<GameLatticeComp>, isClear = true) {
                if (!compList) return;
                compList.forEach((comp) => {
                    if (isClear) {
                        if (comp.imgColor == GameLatticeComp.imgColor.red) {
                            comp.setColor(GameLatticeComp.imgColor.none);
                        }
                    } else {
                        if (comp.imgColor == GameLatticeComp.imgColor.none) {
                            comp.setColor(GameLatticeComp.imgColor.red);
                        }
                    }
                })
            }
            let intervalId = setInterval(() => {
                if (count % 2 == 1) {
                    setItemListColor(runList[idx + 2]);
                    setItemListColor(runList[idx + 3]);
                } else {
                    setItemListColor(runList[idx - 1]);
                    setItemListColor(runList[idx - 2]);
                }
                setItemListColor(runList[idx], false);
                setItemListColor(runList[idx + 1], false);
                if (count % 2 == 0) {
                    idx++;
                } else {
                    idx--;
                }
                if (idx == runList.length || idx == -1) {
                    if (idx == runList.length) idx = runList.length - 1;
                    if (idx == -1) idx = 0;
                    count++;
                    if (count >= 2) {
                        clearInterval(intervalId);
                        setTimeout(() => {
                            resolve(undefined);
                        }, 200);
                    }
                }
            }, 50)
        })
    }
    /**
     * 设置全局的颜色
     * none: 0,
     * red: 7,//红
     * yellow: 1,//黄
     * blue: 8,//蓝
     * green: 2,//绿
     * orange: 6,//橙
     * purple: 5,//紫
     */
    public setAllColor(color: string) {
        this._latticeList.forEach(lattice => {
            lattice.setColor(GameLatticeComp.imgColor[color]);
        })
    }
    /**显示数字配置 */
    public showNumCfg(cfg: game.INumCfgInfo) {
        this._latticeList.forEach((lattice, idx) => {
            const x = idx % 12;
            const y = Math.floor(idx / 12);
            if (y < 2 || x < 2 || y > this._latticeSize.y - 3 || x > 9) {
                lattice.setColor(GameLatticeComp.imgColor.green);
            } else {
                lattice.setColor(GameLatticeComp.imgColor.none);
            }
        })
        let startPos: game.IPos = { x: 0, y: 0 }
        if (cfg.position == 'center') {
            startPos.y = Math.floor((this._latticeSize.y - cfg.list.length) / 2);
        } else if (cfg.position == 'down') {
            startPos.y = 2;
        }
        cfg.list.forEach((info, y) => {
            for (let num = 0; num < info.len; num++) {
                let lattice = this.getLatticeByPos({
                    x: info.x + num,
                    y: y + startPos.y
                })
                lattice.setColor(GameLatticeComp.imgColor[info.color || 'red']);
            }
            if (info.x2 && info.len2) {
                for (let num = 0; num < info.len2; num++) {
                    let lattice = this.getLatticeByPos({
                        x: info.x2 + num,
                        y: y + startPos.y
                    })
                    lattice.setColor(GameLatticeComp.imgColor[info.color || 'red']);
                }
            }
        })
    }
}