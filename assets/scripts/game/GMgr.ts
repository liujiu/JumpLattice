import { sys } from "cc";
import { Log, Singleton } from "../frame";
import { GSound } from "./GSound";

export enum MessageType {
    /**游戏开始（游戏开始时触发） */
    GAME_START = 1,
    /**游戏结束（失败时触发） */
    GAME_OVER = 2,
}

export interface MessageVo {
    type: MessageType;
    data?: any;
}

export interface MessageVo2 {
    type: MessageType;
    code: number;
    data?: any
}

window['changeVoiceSetting'] = function (isClose: boolean) {
    isClose ? GSound.closeVoice() : GSound.openVoice();
}

export class GMgr extends Singleton {
    public sendMessage(data: MessageVo): boolean {
        let dataStr = JSON.stringify(data);
        Log.log('上报:', dataStr);
        if (sys.os == sys.OS.ANDROID || sys.os == sys.OS.LINUX) {
            if (window['Android'] && window['Android'].updateScore) {
                window['Android'].updateScore(dataStr);
                return true;
            }
        } else if (sys.os == sys.OS.IOS) {
            if (window['webkit'] && window['webkit'].messageHandlers && window['webkit'].messageHandlers.updateScore) {
                window['webkit'].messageHandlers.updateScore.postMessage(dataStr);
                return true
            }
        }
        return false
    }
    /**
     * 
     * resLoadSuccess()
     * resLoadFail()
     * resLoadProgress(finished: number, total: number)
     * @param funcName 
     * @param jsonStr 
     * @returns 
     */
    public runNativeFunc(funcName: string, jsonStr?: string | number): boolean {
        Log.log('上报:', funcName, jsonStr);
        if (sys.os == sys.OS.ANDROID || sys.os == sys.OS.LINUX) {
            if (window['Android'] && window['Android'][funcName]) {
                window['Android'][funcName](jsonStr);
                return true;
            }
        } else if (sys.os == sys.OS.IOS) {
            if (window['webkit'] && window['webkit'].messageHandlers && window['webkit'].messageHandlers[funcName]) {
                window['webkit'].messageHandlers[funcName].postMessage(jsonStr);
                return true
            }
        }
        return false
    }
}