
export const GameCfg = {
    num: {
        '3': {
            position: 'center',
            list: [
                { x: 3, len: 6 },
                { x: 3, len: 6 },
                { x: 7, len: 2 },
                { x: 7, len: 2 },
                { x: 3, len: 6 },
                { x: 3, len: 6 },
                { x: 7, len: 2 },
                { x: 7, len: 2 },
                { x: 3, len: 6 },
                { x: 3, len: 6 }
            ]
        },
        '2': {
            position: 'center',
            list: [
                { x: 3, len: 6 },
                { x: 3, len: 6 },
                { x: 3, len: 2 },
                { x: 3, len: 2 },
                { x: 3, len: 6 },
                { x: 3, len: 6 },
                { x: 7, len: 2 },
                { x: 7, len: 2 },
                { x: 3, len: 6 },
                { x: 3, len: 6 }
            ]
        },
        '1': {
            position: 'center',
            list: [
                { x: 5, len: 3 },
                { x: 5, len: 3 },
                { x: 5, len: 3 },
                { x: 5, len: 3 },
                { x: 5, len: 3 },
                { x: 5, len: 3 },
                { x: 5, len: 3 },
                { x: 3, len: 5 },
                { x: 4, len: 4 },
                { x: 5, len: 3 },
                { x: 6, len: 2 }
            ]
        },
        'K': {
            position: 'center',
            list: [
                // { x: 2, len: 8 },
                // { x: 2, len: 8 },
                // { x: 2, len: 0 },
                // { x: 2, len: 0 },
                // { x: 2, len: 0 },
                // { x: 2, len: 0 },
                { x: 3, len: 2, x2: 7, len2: 2, color: 'blue' },
                { x: 3, len: 2, x2: 7, len2: 2, color: 'blue' },
                { x: 3, len: 2, x2: 6, len2: 2, color: 'blue' },
                { x: 3, len: 4, color: 'blue' },
                { x: 3, len: 3, color: 'blue' },
                { x: 3, len: 4, color: 'blue' },
                { x: 3, len: 2, x2: 6, len2: 2, color: 'blue' },
                { x: 3, len: 2, x2: 7, len2: 2, color: 'blue' },
                { x: 3, len: 2, x2: 7, len2: 2, color: 'blue' }
            ]
        }
    }
}