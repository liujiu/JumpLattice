declare namespace game {
    interface INumCfgInfo {
        position: 'center' | 'down',
        list: Array<{
            x: number,
            len: number,
            x2?: number,
            len2?: number,
            color?: string,
        }>
    }
    interface IPos {
        x: number,
        y: number
    }
}
