import { AssetManager, Component, Label, _decorator, resources } from 'cc';
import { DEBUG } from 'cc/env';
import { CommonUtils, Log } from '../frame';
import { GamePresenter } from '../game/GamePresenter';
const { ccclass, property } = _decorator;

/**
 * 
 * create user:jormi
 * time:Mon Oct 24 2022 13:40:29 GMT+0800 (中国标准时间)
 *
 */
@ccclass('LoadingView')
export class LoadingView extends Component {
    private lbl_load: Label;

    public onEnable() {
        CommonUtils.ObjChildrenNameTraversal(this, this.node);
        this.lbl_load.string = '正在加载资源:0%';
        if (DEBUG) {
            this.loadRes();
        } else {
            this.loadSuccess();
        }
    }
    private loadRes() {
        let resList: Array<string> = [];
        //基础配置资源
        Log.log('开始加载资源')
        resources.loadDir('./', (finished: number, total: number, item: AssetManager.RequestItem) => {
            this.lbl_load.string = `正在加载资源:${(finished / total * 100).toFixed(2)}%`;
        }, (err: Error | null, data) => {
            if (err) {
                // MsgView.show('资源加载异常');
                return;
            }
            Log.log('资源加载完成');
            this.loadSuccess();
        })
    }
    /**资源加载成功 */
    private loadSuccess() {
        this.node.destroy();
        GamePresenter.instance().showView();
    }
}