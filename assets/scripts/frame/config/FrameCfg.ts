
export class FrameCfg {
    /**名称缩写 */
    private static nameAbbr = {
        'lbl': 'cc.Label',
        'img': 'cc.Sprite',
        'btn': 'cc.Button',
        'tf': 'cc.UITransform',
        'uibtn': 'UIButton',
        'uiimg': 'UIImage',
        'uilbl': 'UILabel',
        'uiadap': 'UIAdapNode',
        'uibadap': 'UIBindAdapNode',
        'camera': 'cc.Camera',
        'light': 'cc.Light',
        'bar': 'cc.ProgressBar'

    }
    public static getNameAbbr(name: string) {
        return this.nameAbbr[name] || name;
    }
}