import { Component, _decorator } from "cc";
import { UIComponent } from "./UIComponent";

const { ccclass, property, menu } = _decorator;
@ccclass('UITableCell')
@menu('frame/UITableCell')
export class UITableCell extends UIComponent {
    /**单项数据 */
    public data: any;
    /**数据的下标值 */
    public idx: number = -1;
    /**更新布局,这里如果需要改变size的话需要及时的更新 */
    public upLayout() {

    }
}
