
/**
 * 绑定适配节点
 */

import { Component, UITransform, Node, _decorator, Size } from "cc";
import { EDITOR } from "cc/env";

const { ccclass, menu, property, requireComponent, executeInEditMode } = _decorator;

@ccclass('UIBindAdapNode')
@menu('frame/UIBindAdapNode')
@requireComponent(UITransform)
@executeInEditMode
export class UIBindAdapNode extends Component {
    @property({
        type: Node,
        tooltip: '绑定关系的父节点',
    })
    public target: Node;
    @property({
        tooltip: '绑定关系的父节点Size',
    })
    public targetSize: Size = new Size();
    /**缓存大小 */
    public cacheContentSize: Size = new Size();

    public onLoad() {
        this.cacheContentSize = this.getComponent(UITransform).contentSize.clone();
    }
    public onEnable() {
        if (!this.target) return
        if (EDITOR) {
            this.targetSize = this.target.getComponent(UITransform).contentSize.clone();
        } else {
            this.target.on(Node.EventType.SIZE_CHANGED, this.onTargetSizeChanged, this);
            this.onTargetSizeChanged();
        }
    }
    public onDisable() {
        if (EDITOR) return
        this.target.off(Node.EventType.SIZE_CHANGED, this.onTargetSizeChanged, this);
    }
    private onTargetSizeChanged() {
        let transform = this.getComponent(UITransform);
        let targetTransform = this.target.getComponent(UITransform);
        transform.height = this.cacheContentSize.height / this.targetSize.height * targetTransform.height;
        transform.width = this.cacheContentSize.width / this.targetSize.width * targetTransform.width;
    }
}