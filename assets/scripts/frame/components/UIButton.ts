import { _decorator, Button } from 'cc';
const { ccclass, property, menu } = _decorator;

@ccclass('UIButton')
@menu('frame/UIButton')
export class UIButton extends Button {

}