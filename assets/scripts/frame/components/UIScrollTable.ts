import { Component, instantiate, Node, NodeEventType, Prefab, ScrollView, Size, size, UITransform, v2, v3, Vec3, _decorator } from "cc";
import { NodePool } from "../core/NodePool";
import { Log } from "../log/Log";
import { UITableCell } from "./UITableCell";

const { ccclass, menu, property, requireComponent } = _decorator;
@ccclass('UIScrollTable')
@menu('frame/UIScrollTable')
@requireComponent(ScrollView)
export class UIScrollTable extends Component {

    // @property({
    //     tooltip: '',
    // })
    @property(Prefab)
    public cell: Prefab;

    private _scroll: ScrollView;
    private _content: Node;

    private _dataList: Array<any>;
    private _nodePool: NodePool = new NodePool();
    /**当前itemSize */
    private _itemSize: Size;
    /**当前的复数值 */
    private _plural: number = 1;

    /**cell最大展示数 */
    private _maxCellNum: number = 0;
    private _isInit: boolean = false;
    /**是否能滑动 */
    private _isCanMove: boolean = false;
    public onLoad() {
        this._scroll = this.getComponent(ScrollView);
        this._content = this._scroll.content;
        this._content.on(NodeEventType.TRANSFORM_CHANGED, this.transformChanged, this);
    }
    onEnable() {

    }
    /**更新数据，为空的话会自动刷新界面，content的锚点保持0 和0.5 */
    public upData(dataList?: Array<any>) {
        if (typeof dataList == 'object') {
            this._dataList = dataList;
        }
        let children = this._content.children,
            scrollTransform = this._scroll.getComponent(UITransform),
            contentTransform = this._content.getComponent(UITransform),
            contentSize: Size = size();
        this._scroll.stopAutoScroll();
        if (this._scroll.horizontal) {
            this._scroll.scrollToLeft();
            contentSize.height = this._scroll.getComponent(UITransform).contentSize.height;
            contentTransform.anchorPoint = v2(0, 0.5);
        } else if (this._scroll.vertical) {
            this._scroll.scrollToTop();
            contentSize.width = this._scroll.getComponent(UITransform).contentSize.width;
            contentTransform.anchorPoint = v2(0.5, 1);
        }
        //复数项
        let _pluralCellL = 0;
        for (let idx = 0, len = dataList.length; idx < len; idx++) {
            let node = children[idx] || this._nodePool.pop('cell');
            if (!node) {
                node = instantiate(this.cell);
            }
            this._content.addChild(node);
            this.upCellData(node, idx);
            if (!this._itemSize) {
                this._itemSize = node.getComponent(UITransform).contentSize.clone();
                if (this._scroll.horizontal) {
                    this._plural = Math.floor(scrollTransform.height / this._itemSize.height);
                    _pluralCellL = scrollTransform.height / this._plural;
                    this._maxCellNum = (Math.ceil(scrollTransform.width / this._itemSize.width) + 1) * this._plural;
                } else if (this._scroll.vertical) {
                    this._plural = Math.floor(scrollTransform.width / this._itemSize.width);
                    _pluralCellL = scrollTransform.width / this._plural;
                    this._maxCellNum = (Math.ceil(scrollTransform.height / this._itemSize.height) + 1) * this._plural;
                }
            }
            if (this._scroll.horizontal) {
                node.position = v3((Math.floor(idx / this._plural) + 0.5) * this._itemSize.width,
                    (scrollTransform.height - _pluralCellL) / 2 - idx % this._plural * _pluralCellL);
            } else if (this._scroll.vertical) {
                node.position = v3((-scrollTransform.width + _pluralCellL) / 2 + idx % this._plural * _pluralCellL,
                    -(Math.floor(idx / this._plural) + 0.5) * this._itemSize.height);
            }
            //最后一项
            if (idx + 1 == this._maxCellNum) {
                break;
            }
        }
        if (!this._itemSize) return;
        //更新content的大小
        if (this._scroll.horizontal) {
            contentSize.width = this._itemSize.width * Math.ceil(dataList.length / this._plural);
            this._isCanMove = contentSize.width > scrollTransform.contentSize.width;
        } else if (this._scroll.vertical) {
            contentSize.height = this._itemSize.height * Math.ceil(dataList.length / this._plural);
            this._isCanMove = contentSize.height > scrollTransform.contentSize.height;
        }
        contentTransform.contentSize = contentSize;

        for (let idx = children.length - 1, len = dataList.length; idx >= len; idx--) {
            children[idx].removeFromParent();
            this._nodePool.push('cell', children[idx]);
        }
        this._isInit = true;
    }
    /**更新数据 */
    private upCellData(cellNode: Node, idx: number) {
        let tableCell = cellNode.getComponent(UITableCell);
        if (tableCell.data != this._dataList[idx] || tableCell.idx != idx) {
            tableCell.idx = idx;
            tableCell.data = this._dataList[idx];
            tableCell.upLayout();
            return true;
        }
        return false;
    }
    /**
     * 
     * 节点改变位置、旋转或缩放事件。如果具体需要判断是哪一个事件，可通过判断回调的第一个参数类型是 `Node.TransformBit` 中的哪一个来获取
     * @example
     * ```
     * this.node.on(Node.EventType.TRANSFORM_CHANGED, (type)=>{
     *  if (type & Node.TransformBit.POSITION) {
     *       //...
     *   }
     * }, this);
     * ```
     */
    private transformChanged(type) {
        if (type & Node.TransformBit.POSITION && this._isInit && this._isCanMove) {
            let children = this._content.children,
                /**更新的长度 */
                upL = 0,
                pos2 = 0,
                //更新了多少项
                upNum = 0,
                node: Node,
                scrollTransform = this._scroll.getComponent(UITransform),
                unNodeList: Array<Node> = [],
                onkNodeList: Array<Node> = [],
                contentTransform = this._content.getComponent(UITransform);
            if (this._scroll.horizontal) {
                let minX = -scrollTransform.width / 2;
                let maxX = minX - contentTransform.width + this._maxCellNum / this._plural * this._itemSize.width;
                if (this._content.position.x > minX) pos2 = minX;
                else if (this._content.position.x < maxX) pos2 = maxX;
                else pos2 = this._content.position.x;
                upL = pos2 - minX;
                let _pluralCellL = scrollTransform.height / this._plural;
                //首位idx
                upNum = Math.floor(Math.abs(upL) / this._itemSize.width) * this._plural;
                //相同的数据
                if (children[0].getComponent(UITableCell).idx == upNum) return;

                //得到可用的数组
                for (let i = 0, len = children.length; i < len; i++) {
                    node = this.getCellByIdx(i + upNum);
                    if (node) {
                        onkNodeList.push(node);
                    }
                }
                for (let i = 0, len = children.length; i < len; i++) {
                    if (onkNodeList.indexOf(children[i]) == -1)
                        unNodeList.push(children[i]);
                }

                for (let i = 0, len = children.length; i < len; i++) {
                    let upIdx = i + upNum;
                    node = this.getCellByIdx(upIdx, onkNodeList);
                    if (!node) node = unNodeList.pop();
                    if (this.upCellData(node, upIdx)) {
                        node.setSiblingIndex(i);
                        node.position = v3((Math.floor(upIdx / this._plural) + 0.5) * this._itemSize.width,
                            (scrollTransform.height - _pluralCellL) / 2 - upIdx % this._plural * _pluralCellL);
                    };
                }
            } else if (this._scroll.vertical) {
                let minY = scrollTransform.height / 2;
                let maxY = minY + contentTransform.height - this._maxCellNum / this._plural * this._itemSize.height;
                if (this._content.position.y < minY) pos2 = minY;
                else if (this._content.position.y > maxY) pos2 = maxY;
                else pos2 = this._content.position.y;
                upL = pos2 - minY;
                let _pluralCellL = scrollTransform.width / this._plural;
                upNum = Math.floor(Math.abs(upL) / this._itemSize.height) * this._plural;
                //相同的数据
                if (children[0].getComponent(UITableCell).idx == upNum) return;

                //得到可用的数组
                for (let i = 0, len = children.length; i < len; i++) {
                    node = this.getCellByIdx(i + upNum);
                    if (node) {
                        onkNodeList.push(node);
                    }
                }
                for (let i = 0, len = children.length; i < len; i++) {
                    if (onkNodeList.indexOf(children[i]) == -1)
                        unNodeList.push(children[i]);
                }

                for (let i = 0, len = children.length; i < len; i++) {
                    let upIdx = i + upNum;
                    node = this.getCellByIdx(upIdx, onkNodeList);
                    if (!node) node = unNodeList.pop();
                    if (this.upCellData(node, upIdx)) {
                        node.position = v3((-scrollTransform.width + _pluralCellL) / 2 + upIdx % this._plural * _pluralCellL,
                            -(Math.floor(upIdx / this._plural) + 0.5) * this._itemSize.height)
                    };
                }
            }
        }
    }
    private getCellByIdx(idx: number, children?: Array<Node>): Node {
        children = children || this._content.children;
        for (let i = 0, len = children.length; i < len; i++) {
            if (children[i].getComponent(UITableCell).idx == idx) {
                return children[i];
            }
        }
    }
    public onDestroy() {
        this._nodePool.clearFunc('cell', (node: Node) => {
            node.destroy();
        })
    }
}
