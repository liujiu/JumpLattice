import { _decorator, Label, CCString } from 'cc';
const { ccclass, property, menu } = _decorator;

@ccclass('UILabel')
@menu('frame/UILabel')
export class UILabel extends Label {
    
}