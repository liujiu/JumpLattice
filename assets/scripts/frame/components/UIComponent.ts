import { Component, _decorator } from "cc";
import { CommonUtils } from "../utils/CommonUtils";

const { ccclass, property } = _decorator;
@ccclass('UIComponent')
export class UIComponent extends Component {

    /**是否开启节点命名遍历，这样就不需要在视图拖拽了，但是命名必须唯一，否则只能读取做后的，如果不需要的话建议不要开启 */
    @property({
        tooltip: `是否开启节点命名遍历，这样就不需要在视图拖拽了
但是命名必须唯一，否则只能读取最后的，如果不需要的话建议不要开启
注意：命名首位为组件类名，否者获取出来的是节点
`,
    })
    public openNameTraversal = false;

    protected __preload() {
        if (this.openNameTraversal) {
            CommonUtils.ObjChildrenNameTraversal(this, this.node);
        }
    }
    protected onEnable() {
        this.bindEvents();
    }
    protected onDisable() {
        this.removeEvents();
    }
    protected bindEvents() { }
    protected removeEvents() { }
    protected getConstructor() { return (<any>this).__proto__.constructor };
}
