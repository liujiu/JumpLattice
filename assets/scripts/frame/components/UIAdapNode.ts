import { Component, Enum, UITransform, Vec3, _decorator, screen, view, find, Canvas } from "cc";
import { UIUtils } from "../utils/UIUtils";

const { ccclass, menu, property, requireComponent } = _decorator;
export enum AdapNodeType {
    /**背景大小适配适配size */
    BgSize,
    /**背景大小适配缩放 */
    BgScale,
    /**全面屏适配（适配到刘海区域） */
    AllScreen,
    /**海屏适配（不适配到刘海区域） */
    BangScreen,
    /**节点全面屏适配（按照当前节点size等比放大缩小至设计分辨率，且长宽长度等比适配） */
    NodeAdapAll,
    /**节点刘海屏适配（按照当前节点size等比放大缩小至设计分辨率，且长宽长度等比适配） */
    NodeAdapBang,
}
Enum(AdapNodeType);
/**适配组件 */
@ccclass('UIAdap')
@menu('frame/UIAdap')
@requireComponent(UITransform)
export class UIAdapNode extends Component {
    private static bangLength: number = 50;//齐刘海占用大小

    @property({
        type: AdapNodeType,
        displayName: '适配类型',
        tooltip: `BgSize 背景大小适配
        BgScale 背景大小适配
        AllScreen 全面屏适配
        BangScreen 刘海屏适配
        NodeAdapAll 节点全面屏适配
        NodeAdapBang 节点刘海屏适配
        `,
    })
    public adapType: AdapNodeType = AdapNodeType.BangScreen;

    public onLoad() {
        this.onAdap();
    }
    /**
     * 执行适配
     */
    public onAdap() {
        let transform = this.getComponent(UITransform);
        if (!transform) transform = this.addComponent(UITransform);
        let winSize = view.getVisibleSize();
        let _xs = winSize.width / transform.width;
        let _ys = winSize.height / transform.height;
        let _bl: number;
        switch (this.adapType) {
            case AdapNodeType.AllScreen:
                transform.width = winSize.width;
                transform.height = winSize.height;
                break;
            case AdapNodeType.BangScreen:
                if (UIUtils.Orientation == 1) {
                    transform.height = winSize.height;
                    if (UIUtils.isAllScreen()) {
                        transform.width = winSize.width - UIAdapNode.bangLength * 2;
                    } else {
                        transform.width = winSize.width;
                    }
                } else {
                    transform.width = winSize.width;
                    if (UIUtils.isAllScreen()) {
                        transform.height = winSize.height - UIAdapNode.bangLength;
                    } else {
                        transform.height = winSize.height;
                    }
                    this.node.setPosition(new Vec3(this.node.position.x, this.node.position.y - UIAdapNode.bangLength / 2, 0))
                }
                break;
            case AdapNodeType.BgScale:
                _bl = _xs > _ys ? _xs : _ys;
                this.node.setScale(new Vec3(_bl, _bl, 1));
                break;
            case AdapNodeType.BgSize:
                _bl = _xs > _ys ? _xs : _ys;
                transform.width = _bl * transform.width;
                transform.height = _bl * transform.height;
                break;
            case AdapNodeType.NodeAdapAll:
                _bl = _xs < _ys ? _xs : _ys;
                this.node.setScale(new Vec3(_bl, _bl, 1));
                if (_xs < _ys) {
                    transform.height += (winSize.height - transform.height * _bl) / _bl;
                } else {
                    transform.width += (winSize.width - transform.width * _bl) / _bl;
                }
                break;
            case AdapNodeType.NodeAdapBang:
                _bl = _xs < _ys ? _xs : _ys;
                this.node.setScale(new Vec3(_bl, _bl, 1));
                if (UIUtils.Orientation == 1) {
                    if (_xs < _ys) {
                        transform.height += (winSize.height - transform.height * _bl) / _bl;
                    } else {
                        if (UIUtils.isAllScreen()) {
                            transform.width += (winSize.width - transform.width * _bl - UIAdapNode.bangLength * 2) / _bl;
                        } else {
                            transform.width += (winSize.width - transform.width * _bl) / _bl;
                        }
                    }
                } else {
                    if (_xs < _ys) {
                        transform.width += (winSize.width - transform.width * _bl) / _bl;
                    } else {
                        if (UIUtils.isAllScreen()) {
                            transform.height += (winSize.height - transform.height * _bl - UIAdapNode.bangLength) / _bl;
                        } else {
                            transform.height += (winSize.height - transform.height * _bl) / _bl;
                        }
                    }
                }
                break;
        }
    }
}