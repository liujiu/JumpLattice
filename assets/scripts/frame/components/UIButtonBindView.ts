
import { _decorator, Sprite, Component, Button, EventHandler, NodeEventType, js } from 'cc';
const { ccclass, property, menu } = _decorator;

/**
 * 按钮绑定视图
 * 点击会自动显示视图
 */
@ccclass('UIButtonBindView')
@menu('frame/UIButtonBindView')
export class UIButtonBindView extends Component {
    @property({
        tooltip: '视图的类名',
    })
    public className: string = '';

    public onLoad() {
        let button = this.getComponent(Button);
        if (button) {
            let eventHandler = new EventHandler();
            eventHandler.target = this.node;
            eventHandler.component = 'UIButtonBindView';
            eventHandler.handler = 'touchNode';
            button.clickEvents.push(eventHandler);
        } else {
            this.node.on(NodeEventType.TOUCH_END, this.touchNode, this);
        }
    }
    private touchNode() {
        if (!this.className) return;
        let constructor: any = js.getClassByName(this.className);
        if (constructor) {
            constructor.show();
        }
    }
}