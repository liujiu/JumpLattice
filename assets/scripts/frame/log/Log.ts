
export enum LogFontStyle {
    /**警告字体颜色 */
    Warn = 'color:#e6b74d',
    /**异常字体颜色 */
    Error = 'color:#f00',
    /**通用字体颜色 */
    Adopt = 'color:#0fe029',
    /**淡绿字体颜色 */
    LightGreen = 'color:#31dcad',
    /**弱体字体颜色 */
    Slight = 'color:#999999',
}
export class Log {
    private static get isLog() {
        return true;
    }
    public static log(...data: any[]) {
        if (!this.isLog) return;
        console.log(...data);
    }
    public static debug(...data: any[]) {
        if (!this.isLog) return;
        console.debug(...data)
    }
    public static error(...data: any[]) {
        if (!this.isLog) return;
        console.error(...data)
    }
    public static warn(...data: any[]) {
        if (!this.isLog) return;
        console.warn(...data)
    }
    /**socket打印日志 */
    public static socket(value: string) {
        if (!this.isLog) return;
        console.log('%c' + 'value', LogFontStyle.Slight);
    }
    /**打印堆栈信息 */
    public static trace(...params) {
        if (!this.isLog) return;
        const e = new Error('');
        let str = e.stack.toString();
        const list = str.split('\n');
        list[0] += ' Trace';
        list.splice(1, 1);
        console.groupCollapsed(...params);
        console.log(list.join('\n'));
        console.groupEnd();
    }
}

Log.log = console.log;
Log.debug = console.debug;
Log.error = console.error;
Log.warn = console.warn;