import { HashObject } from "../core/HashObject";
import { BaseEvent } from "./BaseEvent";

/**这是一个全局事件管理,如果还有特殊的事件可以继承这个用来区分事件 */
export class Event extends HashObject {
    public static event: BaseEvent = new BaseEvent();
    /**
     * 监听事件
     * @param eventKey 事件名
     * @param callback 方法
     * @param target 绑定对象
     * @param isOnce 是否只执行一次
     */
    public static on(eventKey: string | number, callback: Function, target: Object, isOnce?: boolean) {
        this.event.on(eventKey, callback, target, isOnce);
    }
    public static onOnce(eventKey: string | number, callback: Function, target: Object) {
        this.event.on(eventKey, callback, target, true);
    }
    /**
     * 绑定list事件
     * @param target 绑定对象
     * @param list [[key,this.callback]]
     */
    public static onList(target: Object, list: Array<Array<any>>) {
        list.forEach((v) => {
            this.on(v[0], v[1], target);
        })
    }
    /**
     * 推送事件
     * @param eventKey 事件名
     * @param param 参数
     */
    public static emit(eventKey: string | number, ...param: any) {
        this.event.emit(eventKey, ...param);
    }
    /**
     * 取消监听
     * @param eventKey 事件名
     * @param callback 方法
     * @param target 绑定对象
     */
    public static off(eventKey: string | number, callback: Function, target: Object) {
        this.event.off(eventKey, callback, target);
    }
    /**
     * 取消绑定在对象上的所有事件
     * @param obj 绑定对象
     */
    public static targetOff(target: Object) {
        this.event.targetOff(target);
    }
    /**
     * key的方式移除
     * @param eventName 事件名
     */
    public static keyOff(eventKey: string | number) {
        this.event.keyOff(eventKey);
    }
    /**
     * 清理所有事件
     */
    public static clearEvents() {
        this.event.clear();
    }
}