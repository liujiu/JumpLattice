import { Event } from "./Event";

export class SysEvent extends Event {
    /**显示转圈 */
    public static SHOW_LOADING = 'SysEvent.SHOW_LOADING';
    /**隐藏转圈 */
    public static HIDE_LOADING = 'SysEvent.HIDE_LOADING';
    /**多语言切换 */
    public static LANG_CHANGE = 'SysEvent.LANG_CHANGE';
    /**背景音乐开关 */
    public static MUSIC_ON_OFF = 'SysEvent.MUSIC_ON_OFF';
    /**加载语言资源 */
    public static LOAD_LANG_RES = 'SysEvent.LOAD_LANG_RES';
    /**加载语言资源结束 */
    public static LOAD_LANG_RES_END = 'SysEvent.LOAD_LANG_RES_END';
}