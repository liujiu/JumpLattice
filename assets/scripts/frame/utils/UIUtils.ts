import { find, Node, screen, Vec3 } from "cc";

export class UIUtils {
    /**屏幕方向 1 横屏，2 竖屏 */
    public static get Orientation(): 1 | 2 {
        let viewSize = screen.windowSize;
        if (viewSize.width > viewSize.height) {
            return 1;
        } else {
            return 2;
        }
    }
    /**是否是齐刘海屏幕 */
    public static isAllScreen(): boolean {
        let viewSize = screen.windowSize;
        return viewSize.width / viewSize.height >= 2.0 ||
            viewSize.height / viewSize.width >= 2.0;
    }
    /**获取UI Canvas */
    public static getUICanvas(): Node {
        return find('Canvas');
    }
    /**
     * 已知两点坐标求出距离
     */
    public static formulaDistance(pos1: Vec3, pos2: Vec3): number {
        if (pos1.x == pos2.x) {
            return Math.abs(pos1.y - pos2.y);
        }
        if (pos1.y == pos2.y) {
            return Math.abs(pos1.x - pos2.x);
        }
        let lon = pos1.x - pos2.x;
        let lat = pos1.y - pos2.y;
        return Math.sqrt(lon * lon + lat * lat);
    }
}