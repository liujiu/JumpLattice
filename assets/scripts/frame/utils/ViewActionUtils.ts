import { tween, Node, v3 } from "cc";

export class ViewActionUtils {
    /**放大缩小动画 */
    public static playShowViewAction1(node: Node): Promise<Node> {
        return new Promise((resolve, reject) => {
            node.scale = v3(0.5, 0.5, 1);
            tween(node)
                .to(0.1, {
                    scale: v3(1.1, 1.1, 1)
                })
                .to(0.1, {
                    scale: v3(1, 1, 1)
                })
                .call(() => {
                    resolve(node);
                })
                .start()
        });
    }
}