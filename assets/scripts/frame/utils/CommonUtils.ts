import { sys, Node, native } from "cc";
import { FrameCfg } from "../config/FrameCfg";

export class CommonUtils {
    public static cloneJSON(obj: Object) {
        if (!obj) return obj;
        return JSON.parse(JSON.stringify(obj));
    }
    /**gcj02坐标转百度坐标 */
    public static gcj02_To_Bd09(gg_lon, gg_lat) {
        let pi = 3.141592653589793 * 3000.0 / 180.0;
        let x = gg_lon, y = gg_lat;
        let z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * pi);
        let theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * pi);
        let bd_lon = z * Math.cos(theta) + 0.0065;
        let bd_lat = z * Math.sin(theta) + 0.006;
        return {
            bd_lon: bd_lon,
            bd_lat: bd_lat
        };
    }
    /**
     * 前置加0
     * @param num 数字
     * @param length 最大位数
     */
    public static prefixInteger(num: number, length: number = 2) {
        return (Array(length).join('0') + num).slice(-length);
    }
    /**秒转 天 时 分 秒*/
    public static secondsToDHMS(s: number) {
        let _d = Math.floor(s / (60 * 60 * 24));
        let _h = Math.floor((s / (60 * 60)) % 24);
        let _ms = Math.floor((s / 60) % 60);
        let _s = Math.floor(s % 60);
        let str = '';
        if (_d > 0) {
            str += _d + '天';
        }
        if (_h > 0) {
            str += _h + '时';
        }
        if (_ms > 0) {
            str += this.prefixInteger(_ms) + '分';
        }
        str += this.prefixInteger(_s) + '秒';
        return str;
    }
    /**秒转 时 分 秒*/
    public static secondsToHMS(s: number) {
        if (s == 0) return '00:00:00'
        let _h = Math.floor((s / (60 * 60)));
        let _ms = Math.floor((s / 60) % 60);
        let _s = Math.floor(s % 60);
        return this.prefixInteger(_h) + ':' + this.prefixInteger(_ms) + ':' + this.prefixInteger(_s);
    }
    /**秒表*/
    public static Stopwatch(s: number) {
        if (s == 0) return '00:00:00'
        let _ms = Math.floor((s / (60 * 1000)));
        let _s = Math.floor(s / 1000 % 60);
        let _n = Math.floor((s / 10) % 100)
        return this.prefixInteger(_ms) + ':' + this.prefixInteger(_s) + ':' + this.prefixInteger(_n);
    }
    /**获取现在的时间 */
    public static getNowTime(): string {
        let _time = new Date();
        let _y = _time.getFullYear();
        let _m = _time.getMonth() + 1;
        let _d = _time.getDate();
        let _h = _time.getHours();
        let _ms = _time.getMinutes();
        let _s = _time.getSeconds();
        return _y + '-' + _m + '-' + _d + ' ' + _h + ':' + _ms + ':' + _s;
    }
    /**
     * 获取设备唯一标识
     */
    private static _uuid: string;
    public static generateUUID(): string {
        if (this._uuid) return this._uuid;
        let uuid = "";
        if (!sys.isNative) {
            uuid = sys.localStorage.getItem('generateUUID');
            if (!uuid) {
                let d = new Date().getTime();
                uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                    let r = (d + Math.random() * 16) % 16 | 0;
                    d = Math.floor(d / 16);
                    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
                });
                sys.localStorage.setItem('generateUUID', uuid);
            }
        } else {
            uuid = this.toNative('getUDID');
        }
        this._uuid = uuid;
        return this._uuid;
    }
    public static toNative(methodName: string, param?: any): any {
        if (!sys.isNative) return undefined;
        let className, methodSignature, paramStr;
        if (typeof param == 'string') {
            paramStr = param;
        } else if (typeof param == 'object') {
            paramStr = JSON.stringify(param);
        } else if (typeof param == 'number') {
            paramStr = param.toString();
        }
        if (sys.os == sys.OS.ANDROID) {
            className = 'bridge/JsToJava';
        } else if (sys.os == sys.OS.IOS) {
            className = 'JsToOc';
        }
        if (paramStr) {
            methodSignature = '(Ljava/lang/String;)Ljava/lang/String;'
        } else {
            methodSignature = '()Ljava/lang/String;'
        }
        return native.reflection.callStaticMethod(className, methodName, methodSignature, paramStr);
    }
    /**
     * 子节点名称解析
     * @param obj 类对象
     * @param node 解析节点
     */
    public static ObjChildrenNameTraversal(obj: any, node: Node) {
        node.children.forEach((v) => {
            let clsName = v.name.split('_')[0];
            obj[v.name] = v.getComponent(FrameCfg.getNameAbbr(clsName)) || v;
            if (v.children.length > 0) {
                this.ObjChildrenNameTraversal(obj, v);
            }
        })
    }
}