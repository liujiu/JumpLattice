
export class StringUtils {
    private static base64Key: string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    /**
     * 拷贝对象
     * @param obj 对象
     */
    public static copyObj<T>(obj: T): T {
        if (obj === null) {
            return null;
        }
        return JSON.parse(JSON.stringify(obj));
    }
    /**
     * 根据身份证号计算年龄
     * @param UUserCard 身份证号
     */
    public static AgeIdCard(UUserCard: string): number {
        var myDate = new Date();
        var month = myDate.getMonth() + 1;
        var day = myDate.getDate();
        var age = myDate.getFullYear() - Number(UUserCard.substring(6, 10)) - 1;
        if (Number(UUserCard.substring(10, 12)) < month || Number(UUserCard.substring(10, 12)) == month && Number(UUserCard.substring(12, 14)) <= day) {
            age++;
        }
        return age;
    }
    /**
     * 判断当前字符串是否是URL
     * @param url 
     */
    public static isURL(url: string): boolean {
        return typeof url == 'string' && (url.indexOf('http://') != -1 || url.indexOf('https://') != -1);
    }
    /**
     * 截取字符串
     * @param str 截取字符
     * @param maxC 最大截取数
     * @param endstr 替换字符
     */
    public static captureStr(str: string, maxC: number = 6, endstr: string = ''): string {
        let _str = '';
        if (!str || str == '') return _str
        let len = 0;
        for (let i = 0; i < str.length; i++) {
            let c = str.charCodeAt(i);
            if ((c >= 0x0001 && c <= 0x007e) || (0xff60 <= c && c <= 0xff9f)) {
                len++;
            } else {
                len += 2;
            }
            if (len > 2 * maxC) {
                _str += endstr;
                break;
            }
            _str += str[i];
        }
        return _str;
    }
    /**
     * 为字符串去前后空格
     * @param text 字符串
     */
    public static trim(text) {
        if (typeof (text) == 'string') {
            return text.replace(/^\s*|\s*$/g, '');
        } else {
            return text;
        }
    }
    /**
     * 
     * @param url 链接
     * @param key key
     * @param value 值
     */
    public static addUrlParam(url: string, key, value) {
        value = encodeURIComponent(value);
        if (/\?/g.test(url)) {
            var reg = new RegExp('[?|&]' + key + '=([^&]*)', 'g');
            var res = reg.exec(url);
            if (res) {
                url = url.substr(0, res.index) + res[0].replace(res[1], value) + url.substr(res.index + res[0].length);
            } else {
                url += "&" + key + "=" + value;
            }
        } else {
            url += "?" + key + "=" + value;
        }
        return url;
    }
    /**
     * base64加密
     * @param str 
     */
    public static encodeBase64(input: string): string {
        input = escape(input);
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;
        while (i < input.length) {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);
            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;
            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }
            output = output +
                this.base64Key.charAt(enc1) + this.base64Key.charAt(enc2) +
                this.base64Key.charAt(enc3) + this.base64Key.charAt(enc4);
        }
        return output;
    }
    /**
     * base64解密
     * @param str 
     */
    public static decodeBase64(input: string): string {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;
        while (i < input.length) {
            enc1 = this.base64Key.indexOf(input.charAt(i++));
            enc2 = this.base64Key.indexOf(input.charAt(i++));
            enc3 = this.base64Key.indexOf(input.charAt(i++));
            enc4 = this.base64Key.indexOf(input.charAt(i++));
            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;
            output = output + String.fromCharCode(chr1);
            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }
        }
        output = unescape(output);
        return output;
    }
    /**
     * utf8转string
     * @param array 
     * @returns 
     */
    public static utf8ArrayToStr = function (array: Uint8Array) {
        var out, i, len, c;
        var char2, char3;

        out = "";
        len = array.length;
        i = 0;
        while (i < len) {
            c = array[i++];
            switch (c >> 4) {
                case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7:
                    // 0xxxxxxx
                    out += String.fromCharCode(c);
                    break;
                case 12: case 13:
                    // 110x xxxx   10xx xxxx
                    char2 = array[i++];
                    out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
                    break;
                case 14:
                    // 1110 xxxx  10xx xxxx  10xx xxxx
                    char2 = array[i++];
                    char3 = array[i++];
                    out += String.fromCharCode(((c & 0x0F) << 12) |
                        ((char2 & 0x3F) << 6) |
                        ((char3 & 0x3F) << 0));
                    break;
            }
        }

        return out;
    }
    /**
     * string 转 Uint8Array
     * @param string string
     */
    public static stringToUint8Array(string: string): Uint8Array {
        let pos = 0;
        const len = string.length;

        let at = 0;  // output position
        let tlen = Math.max(32, len + (len >> 1) + 7);  // 1.5x size
        let target = new Uint8Array((tlen >> 3) << 3);  // ... but at 8 byte offset

        while (pos < len) {
            let value = string.charCodeAt(pos++);
            if (value >= 0xd800 && value <= 0xdbff) {
                // high surrogate
                if (pos < len) {
                    const extra = string.charCodeAt(pos);
                    if ((extra & 0xfc00) === 0xdc00) {
                        ++pos;
                        value = ((value & 0x3ff) << 10) + (extra & 0x3ff) + 0x10000;
                    }
                }
                if (value >= 0xd800 && value <= 0xdbff) {
                    continue;  // drop lone surrogate
                }
            }

            // expand the buffer if we couldn't write 4 bytes
            if (at + 4 > target.length) {
                tlen += 8;  // minimum extra
                tlen *= (1.0 + (pos / string.length) * 2);  // take 2x the remaining
                tlen = (tlen >> 3) << 3;  // 8 byte offset

                const update = new Uint8Array(tlen);
                update.set(target);
                target = update;
            }

            if ((value & 0xffffff80) === 0) {  // 1-byte
                target[at++] = value;  // ASCII
                continue;
            } else if ((value & 0xfffff800) === 0) {  // 2-byte
                target[at++] = ((value >> 6) & 0x1f) | 0xc0;
            } else if ((value & 0xffff0000) === 0) {  // 3-byte
                target[at++] = ((value >> 12) & 0x0f) | 0xe0;
                target[at++] = ((value >> 6) & 0x3f) | 0x80;
            } else if ((value & 0xffe00000) === 0) {  // 4-byte
                target[at++] = ((value >> 18) & 0x07) | 0xf0;
                target[at++] = ((value >> 12) & 0x3f) | 0x80;
                target[at++] = ((value >> 6) & 0x3f) | 0x80;
            } else {
                // FIXME: do we care
                continue;
            }

            target[at++] = (value & 0x3f) | 0x80;
        }

        return target.slice(0, at);
    }
    /**
     * 
     * @param str 
     * @returns 
     */
    public static utf8to16(str: string): string {
        var out, i, len, c;
        var char2, char3;
        out = "";
        len = str.length;
        i = 0;
        while (i < len) {
            c = str.charCodeAt(i++);
            switch (c >> 4) {
                case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7:
                    // 0xxxxxxx
                    out += str.charAt(i - 1);
                    break;
                case 12: case 13:
                    // 110x xxxx 10xx xxxx
                    char2 = str.charCodeAt(i++);
                    out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
                    break;
                case 14:
                    // 1110 xxxx 10xx xxxx 10xx xxxx
                    char2 = str.charCodeAt(i++);
                    char3 = str.charCodeAt(i++);
                    out += String.fromCharCode(((c & 0x0F) << 12) |
                        ((char2 & 0x3F) << 6) |
                        ((char3 & 0x3F) << 0));
                    break;
            }
        }
        return out;
    }
    /**
     * 格式化字符串 "{0},{1}.format("text0","text1")
     */
    public static format(val: string, ...param: any[]): string {
        for (let i = 0, len = param.length; i < len; i++) {
            let reg = new RegExp("({)" + i + "(})", "g");
            val = val.replace(reg, param[i]);
        }
        return val;
    }
    /**字符串省略... */
    public static beautySub(str, len) {
        var reg = /[\u4e00-\u9fa5\,.，。-]/g, slice = str.substring(0, len),
            cCharNum = (~~(slice.match(reg) && slice.match(reg).length)),
            realen = slice.length * 2 - cCharNum;
        return str.substr(0, realen) + (realen < str.length ? "..." : "");
    }
}