

import { Asset, assetManager, AssetManager, ImageAsset, isValid, resources, SceneAsset, Sprite, SpriteFrame, Texture2D } from "cc"
import { StringUtils } from "./StringUtils"

type loadBundleParam = {
    bundleName: string,
    success?: (bundle: AssetManager.Bundle) => void,
    fail?: (err) => void,
}
type loadBundleResParam<T extends Asset> = {
    bundleName: string,
    path: string,
    type?: typeof Asset,
    onProgress?: (finish: number, total: number, item: AssetManager.RequestItem) => void,
    success?: (assets: T | Array<T>) => void,
    fail?: (err) => void,
}
type preloadBundleResParam = {
    bundleName: string,
    path: string,
    type?: typeof Asset,
    onProgress?: (finish: number, total: number, item: AssetManager.RequestItem) => void,
    success?: (items?: AssetManager.RequestItem[]) => void,
    fail?: (err) => void,
}
type loadBundleSceneParam = {
    bundleName: string,
    path: string,
    onProgress?: (finish: number, total: number, item: AssetManager.RequestItem) => void,
    success?: (sceneAsset: SceneAsset) => void,
    fail?: (err) => void,
}

interface loadSpriteFrameIF {
    sprite?: Sprite,
    url: string,
    isTexture?: boolean,
    success?: (spriteFrame: SpriteFrame) => void,
    fail?: (path: string) => void,
}

export class ResourceUtils {
    /**
     * 加载精灵视图
     * @param option loadSpriteFrameIF
     */
    public static loadSpriteFrame(option: loadSpriteFrameIF) {
        //防止资源替换冲突
        (option.sprite as any)._loadUrl = option.url;
        //外部链接
        if (StringUtils.isURL(option.url)) {
            assetManager.loadRemote(option.url, (err, texture: Texture2D) => {
                if (isValid(option.sprite, true)) {
                    if ((option.sprite as any)._loadUrl != option.url) return;
                    if (!err && texture) {
                        let spriteframe = new SpriteFrame();
                        spriteframe.texture = texture
                        option.sprite.spriteFrame = spriteframe;
                        option.success && option.success(spriteframe);
                    } else {
                        option.fail && option.fail(option.url);
                    }
                }
            });
        } else {
            let bundleName: string, url: string;
            if (option.url.indexOf(':') != -1) {
                let arr: Array<string> = option.url.split(':');
                bundleName = arr[0];
                url = arr[1];
            } else {
                bundleName = 'resources'
                url = option.url;
            }
            if (url == '') return;
            let loadType = option.isTexture ? SpriteFrame : Texture2D;
            this.loadBundleRes({
                bundleName: bundleName,
                path: option.url,
                type: loadType,
                success: (data: Texture2D | SpriteFrame) => {
                    if (isValid(option.sprite, true)) {
                        if ((option.sprite as any)._loadUrl != option.url) return;
                        let spriteFrame: SpriteFrame;
                        if (data instanceof Texture2D) {
                            spriteFrame = new SpriteFrame();
                            spriteFrame.texture = data;
                        } else {
                            spriteFrame = data;
                        }
                        option.sprite.spriteFrame = spriteFrame;
                        option.success && option.success(spriteFrame);
                    }
                },
                fail: (err: any) => {
                    if (isValid(option.sprite, true)) {
                        if ((option.sprite as any)._loadUrl != option.url) return;
                        option.fail && option.fail(option.url);
                    }
                }
            })
        }
    }
    /**
     * 加载bundle
     * @param option loadBundleParam 类型
     */
    public static loadBundle(option: loadBundleParam) {
        if (!option.bundleName) {
            option.success && option.success(resources);
            return;
        }
        let bundle: AssetManager.Bundle = assetManager.getBundle(option.bundleName);
        if (bundle) {
            option.success(bundle);
        } else {
            assetManager.loadBundle(option.bundleName, null, (err, bundle) => {
                if (err) {
                    option.fail && option.fail(err);
                } else {
                    option.success && option.success(bundle);
                }
            });
        }
    }
    /**
     * 加载bundle 资源
     * @param option loadBundleParam 类型
     */
    public static loadBundleRes(option: loadBundleResParam<Asset>) {
        this.loadBundle({
            bundleName: option.bundleName,
            success: (bundle: AssetManager.Bundle) => {
                let url = option.path;
                if (option.type == SpriteFrame) {
                    if (url.indexOf('/spriteFrame') == -1) {
                        url += '/spriteFrame';
                    }
                } else if (option.type == Texture2D) {
                    if (url.indexOf('/texture') == -1) {
                        url += '/texture';
                    }
                }
                let asset = bundle.get(url, option.type);
                if (asset) {
                    option.success && option.success(asset);
                    return;
                }
                bundle.load(url, option.type,
                    (finish, total, item) => {
                        //加载过程
                        option.onProgress && option.onProgress(finish, total, item);
                    },
                    (err, assets: Asset) => {
                        //加载成功
                        if (err) {
                            option.fail && option.fail(err);
                        } else {
                            option.success && option.success(assets);
                        }
                    });
            },
            fail: option.fail,
        })
    }
    /**
     * 预加载bundle 资源
     * @param option 
     */
    public static preloadBundleRes(option: preloadBundleResParam) {
        this.loadBundle({
            bundleName: option.bundleName,
            success: (bundle: AssetManager.Bundle) => {
                if (option.type === SceneAsset) {
                    bundle.preloadScene(option.path, option.type,
                        (finish, total, item) => {
                            //加载过程
                            option.onProgress && option.onProgress(finish, total, item);
                        },
                        (err: Error) => {
                            //加载成功
                            if (err) {
                                option.fail && option.fail(err);
                            } else {
                                option.success && option.success();
                            }
                        })
                } else {
                    bundle.preload(option.path, option.type,
                        (finish, total, item) => {
                            //加载过程
                            option.onProgress && option.onProgress(finish, total, item);
                        },
                        (err: Error, items: AssetManager.RequestItem[]) => {
                            //加载成功
                            if (err) {
                                option.fail && option.fail(err);
                            } else {
                                option.success && option.success(items);
                            }
                        });
                }
            },
            fail: option.fail,
        })
    }
    /**
     * 加载场景
     * @param option 
     */
    public static loadBundleScene(option: loadBundleSceneParam) {
        this.loadBundle({
            bundleName: option.bundleName,
            success: (bundle: AssetManager.Bundle) => {
                bundle.preloadScene(option.path,
                    (finish: number, total: number, item: AssetManager.RequestItem) => {
                        //加载过程
                        option.onProgress && option.onProgress(finish, total, item);
                    },
                    (err: Error) => {
                        if (err) {
                            option.fail && option.fail(err);
                            return;
                        }
                        //切换场景
                        bundle.loadScene(
                            option.path,
                            (err: Error, sceneAsset: SceneAsset) => {
                                //加载结果
                                if (err) {
                                    option.fail && option.fail(err);
                                } else {
                                    option.success && option.success(sceneAsset);
                                }
                            }
                        );
                    })
            },
            fail: option.fail,
        })
    }
    public static getSpriteframe(path: string, suffix = '/spriteFrame', bundle?: AssetManager.Bundle): SpriteFrame {
        bundle = bundle || resources;
        let imageAsset = bundle.get(path, ImageAsset),
            spriteFrame: SpriteFrame;
        if (!imageAsset) {
            spriteFrame = bundle.get(path + suffix, SpriteFrame);
            if (!spriteFrame) throw new Error("资源加载失败：" + path);
            return spriteFrame;
        }
        let texture = new Texture2D();
        texture.image = imageAsset;
        texture.setWrapMode(Texture2D.WrapMode.CLAMP_TO_EDGE, Texture2D.WrapMode.CLAMP_TO_EDGE);
        spriteFrame = new SpriteFrame();
        spriteFrame.texture = texture;
        return spriteFrame;
    }
}