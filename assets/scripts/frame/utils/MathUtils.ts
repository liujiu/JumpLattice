export class MathUtils {
    /**
     * 生成随机数 四舍五入计算
     * @param min 最小值
     * @param max 最大值
     */
    public static random(min: number, max: number): number {
        return Math.round(Math.random() * (max - min) + min);
    }
    /**
     * 获取一个区间的随机数
     * @param from 最小值
     * @param end 最大值
     * @returns {number}
     */
    public static limit(from: number, end: number): number {
        from = Math.min(from, end);
        end = Math.max(from, end);
        var range: number = end - from;
        return from + Math.random() * range;
    }

    /**
     * 获取一个区间的随机数(整数)
     * @param from 最小值
     * @param end 最大值
     * @returns {number}
     */
    public static limitInteger(from: number, end: number): number {
        return Math.floor(this.limit(from, end + 1));
    }

    /**
     * 在一个数组中随机获取一个元素
     * @param arr 数组
     * @returns {any} 随机出来的结果
     */
    public randomArray(arr: Array<any>): any {
        if (!arr) {
            return null;
        }
        var index: number = Math.floor(Math.random() * arr.length);
        return arr[index];
    }
    /*判断num是否为一个整数*/
    public static isInteger(num: number) {
        return Math.floor(num) === num;
    }
    /**
    * 将一个浮点数转换成整数，返回整数和倍数
    * 如 3.14 》》314 倍数是100
    *
    */
    public static toInteger(floatNum: number) {
        var ret = { times: 1, num: 0 };
        //是整数
        if (this.isInteger(floatNum)) {
            ret.num = floatNum;
            return ret;
        }
        var strfi = floatNum + '';
        //查找小数点的下标
        var dotPos = strfi.indexOf('.');
        //获取小数的位数
        var len = strfi.substr(dotPos + 1).length;
        //Math.pow(10,len)指定10的len次幂。
        var time = Math.pow(10, len);
        //将浮点数转化为整数
        var intNum = parseInt((floatNum * time + 0.5).toString(), 10);
        ret.times = time;
        ret.num = intNum;
        return ret;
    }
    /**
     * 计算公式 = - * /
     * 三个参数分别是要运算的两个数和运算符
     * 因为js 0.1+0.2运算是错误的
     * @param a 
     * @param b 
     * @param op 运算符 
     */
    public static operation(a: number, b: number, op: '+' | '-' | '*' | '/') {
        var o1 = this.toInteger(a);
        var o2 = this.toInteger(b);
        var n1 = o1.num;
        var n2 = o2.num;
        var t1 = o1.times;
        var t2 = o2.times;
        var max = t1 > t2 ? t1 : t2;
        var result = null;
        switch (op) {
            case '+':
                if (t1 === t2) {
                    result = n1 + n2;
                } else if (t1 > t2) {
                    result = n1 + n2 * (t1 / t2);
                } else {
                    result = n1 * (t2 / t1) + n2;
                }
                return result / max;
            case '-':
                if (t1 === t2) {
                    result = n1 - n2;
                } else if (t1 > t2) {
                    result = n1 - n2 * (t1 / t2);
                } else {
                    result = n1 * (t2 / t1) - n2;
                }
                return result / max;
            case '*':
                result = (n1 * n2) / (t1 * t2);
                return result;
            case '/':
                result = (n1 / n2) / (t2 / t1);
                return result;
        }
    }
    /**
     * 数字转化为万 千万 忆
     * @param num 数字
     * @param w 位数
     */
    public static numConversion(num: number, w: number = 2): string {
        num = num || 0;
        let _f = '';
        if (num < 0) {
            _f = '-';
        }
        num = Math.abs(num);
        let cz = function (n, a, b) {
            let bb = this.operation(n / a, Math.floor(n / a), '-');
            let pow = Math.pow(10, b)
            if (bb * pow > 0) {
                return (Math.floor(this.operation(n / a, pow, '*')) / pow).toString();
            } else {
                return Math.floor(n / a);
            }
        }
        if (typeof num == 'string') {
            num = parseInt(num);
        }
        if (num >= 100000000) {
            return _f + cz(num, 100000000, w) + '亿';
        }
        if (num >= 10000) {
            return _f + cz(num, 10000, w) + '万';
        }
        return _f + num.toString();
    }
}