import { HashObject } from "./HashObject";

/**
 * 单例模式基类
 */
export class Singleton extends HashObject {
    /**该对象是否可用 */
    public isValid: boolean = true;
    /**得到一个实例 */
    public static instance<T extends {}>(this: new () => T): T {
        if (!(<any>this)._instance) {
            (<any>this)._instance = new this();
            (<any>this)._instance._ctor_();
        }
        return (<any>this)._instance;
    }
    /**这里可以初始化单例需要做的一些事情 */
    protected _ctor_() { }
    /**是否已经存在的实例 */
    public static isInstance(): boolean {
        return !!(<any>this)._instance;
    }
    /**销毁单例 */
    public static destroy() {
        if (!(<any>this)._instance) return;
        (<any>this)._instance.onDestroy();
        (<any>this)._instance.isValid = false;
        delete (<any>this)._instance;
    }
    /**执行销毁函数 */
    protected onDestroy() { }
}