let hashCount: number = 1;
/**hask对象作为基础对象在使用 */
export class HashObject {
    private _hashCode: number;
    /**
     * 创建一个 HashObject 对象
     */
    public constructor() {
        this._hashCode = hashCount++;
    }
    /**
     * 返回此对象唯一的哈希值,用于唯一确定一个对象。hashCode为大于等于1的整数。
     */
    public get hashCode(): number {
        return this._hashCode;
    }
}