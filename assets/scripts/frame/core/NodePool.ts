import { HashObject } from "./HashObject";

/**节点池子 */
export class NodePool extends HashObject {
    public static _pool = new NodePool();

    private _content: { [key: string]: Array<any> } = {};
    /**添加一个元素 */
    public push(name: string, node: any) {
        if (!this._content[name]) {
            this._content[name] = [];
        }
        let index = this._content[name].indexOf(node);
        if (index == -1) {
            this._content[name].push(node);
        }
    }
    /**取出一个元素 */
    public pop<T>(name: string, func?: (n: T) => boolean): T {
        var list: Array<any> = this._content[name];
        if (list && list.length) {
            if (func) {
                for (let i = 0, len = list.length; i < len; i++) {
                    if (func(list[i])) {
                        return list[i];
                    }
                }
            }
            return list.pop();
        }
        return null;
    }
    /**清除某一项的所有数据，执行回调方法 */
    public clearFunc(name: string, callback: (item) => void) {
        var list: Array<any> = this._content[name];
        if (list && list.length) {
            list.forEach((node) => {
                callback(node);
            })
        }
        delete this._content[name];
    }
    /**添加一个元素 */
    public static push(name: string, node: any) {
        this._pool.push(name, node);
    }
    /**取出一个元素 */
    public static pop<T>(name: string, func?: (n: T) => boolean): T {
        return this._pool.pop(name, func);
    }
    /**清除某一项的所有数据，执行回调方法 */
    public static clearFunc(name: string, callback: (item) => void) {
        this._pool.clearFunc(name, callback);
    }
}