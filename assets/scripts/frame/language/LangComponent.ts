import { Component, Label, Sprite, _decorator } from "cc";
import { SysEvent } from "../event/SysEvent";
import { LangMgr, LangType } from "./LangMgr";

const { ccclass, property, menu } = _decorator;
@ccclass('LangComponent')
@menu('frame/LangComponent')
export class LangComponent extends Component {
    @property({
        tooltip: '多语言Code',
    })
    get langCode(): string {
        return this._langCode;
    }
    set langCode(value: string) {
        if (this._langCode == value) return;
        this._langCode = value;
        this.upText();
    }
    @property({ serializable: true })
    private _langCode: string = '';

    private _label: Label;
    private _image: Sprite;

    public __preload() {
        this.initBase();
    }
    /**初始化基础模块 */
    public initBase() {
        this._label = this.getComponent(Label);
        this._image = this.getComponent(Sprite);
    }
    public onEnable() {
        SysEvent.onList(this, [
            [SysEvent.LOAD_LANG_RES_END, this.langChange]
        ]);
        this.upText();
        this.upSprite();
    }
    private langChange(value: LangType) {
        this.upText();
    }
    /**更新文本 */
    private upText() {
        if (!this._label) return;
        if (this._langCode) {
            this._label.string = LangMgr.instance().langStr(this._langCode);
        }
    }
    /**更新精灵 */
    private upSprite() {
        if (!this._image) return;

    }
    public onDisable() {
        SysEvent.targetOff(this);
    }
    public onDestroy() {
        SysEvent.targetOff(this);
    }
}