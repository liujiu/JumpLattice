
/**
 * 多语言管理器
 * 
 */

import { JsonAsset } from "cc";
import { Singleton } from "../core/Singleton";
import { SysEvent } from "../event/SysEvent";
import { Log } from "../log/Log";
import { ResourceUtils } from "../utils/ResourceUtils";


/**语言类型 */
export enum LangType {
    /**中文 */
    cn = 'cn',
    /**英文 */
    en = 'en'
}
export class LangMgr extends Singleton {
    /**多语言的map */
    private _langMap: { [key: string]: string } = {};
    /**获取到多语言的字符串 */
    public langStr(code: string) {
        return this._langMap[code] || '????';
    }
    protected _ctor_() {
        SysEvent.on(SysEvent.LANG_CHANGE, this.langChange, this);
    }
    /**多语言切换完成，在这里需要移除原有的多语言资源，添加新的多语言资源 */
    private langChange(languageType: LangType) {
        ResourceUtils.loadBundleRes({
            bundleName: 'resource',
            path: `language/${languageType}/${languageType}`,
            type: JsonAsset,
            success(asset) {
                Log.log(asset);
            }
        })
    }
    protected onDestroy(): void {
        SysEvent.off(SysEvent.LANG_CHANGE, this.langChange, this);
    }
}