import { native, sys } from "cc";

export class File {
    /**
     * 获取文件字符串数据
     * @param filename 
     */
    public static getStringFromFile(filename: string): string {
        if (sys.isNative) {
            if (this.isFileExist(filename)) {
                return native.fileUtils.getStringFromFile(filename);
            } else {
                return null;
            }
        }
        return null;
    }
    /**
     * 文件是否存在
     * @param filename 相对路径或者绝对路径 相对路径会去搜索路径优先选择
     */
    public static isFileExist(filename): boolean {
        if (sys.isNative) {
            return native.fileUtils.isFileExist(filename);
        }
        return false;
    }
    /**
     * 是否为绝对路径
     * @param path 路径
     */
    public static isAbsolutePath(path: string): boolean {
        if (sys.isNative) {
            return native.fileUtils.isAbsolutePath(path);
        }
        return false;
    }
    /**
     * 写入数据字符串
     * @param dataStr 字符串数据
     * @param fullPath 路径
     */
    public static writeStringToFile(dataStr: string, fullPath: string): boolean {
        if (sys.isNative) {
            return native.fileUtils.writeStringToFile(dataStr, fullPath);
        }
        return false;
    }
    /**
     * 目录是否存在
     * @param assetsPath 目录
     */
    public static isDirectoryExist(assetsPath: string): boolean {
        if (sys.isNative) {
            return native.fileUtils.isDirectoryExist(assetsPath);
        }
        return false;
    }
    /**
     * 创建目录
     */
    public static createDirectory(assetsPath: string): boolean {
        if (sys.isNative) {
            return native.fileUtils.createDirectory(assetsPath) == 'true';
        }
        return false;
    }
    /**
     * 删除一个目录
     * @param dirPath 
     */
    public static removeDirectory(dirPath: string): boolean {
        if (sys.isNative) {
            return native.fileUtils.removeDirectory(dirPath);
        }
        return false;
    }
    /**获取可写路径 */
    public static getWritablePath(): string {
        if (sys.isNative) {
            return native.fileUtils.getWritablePath();
        }
        return '';
    }
    /**获取根路径 */
    public static getDefaultResourceRootPath(): string {
        if (sys.isNative) {
            return native.fileUtils.getDefaultResourceRootPath();
        }
        return '';
    }
    /**删除一个文件 */
    public static removeFile(filepath: string): boolean {
        if (sys.isNative) {
            return native.fileUtils.removeFile(filepath);
        }
        return false;
    }
    /**添加搜索路径 */
    public static addSearchPath(path: string, front: boolean = false) {
        if (sys.isNative) {
            let array = this.getSearchPaths();
            if (array.indexOf(path) == -1) {
                native.fileUtils.addSearchPath(path, front);
            }
        }
    }
    /**设置搜索路径 */
    public static setSearchPaths(searchPaths: Array<string>) {
        if (sys.isNative) {
            native.fileUtils.setSearchPaths(searchPaths);
        }
    }
    /**获取搜索路径 */
    public static getSearchPaths(): Array<string> {
        if (sys.isNative) {
            return native.fileUtils.getSearchPaths();
        }
    }
}