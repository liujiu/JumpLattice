import { sys, _decorator } from "cc";
import { Singleton } from "../core/Singleton";

const { ccclass } = _decorator;

@ccclass('BaseData')
export abstract class BaseData extends Singleton {
    /**本地数据 */
    protected abstract localData;
    private _localKey: string;
    private get localKey(): string {
        if (!this._localKey) {
            let libraryName = this.getLibraryName();
            let tableName = this.getTableName();
            if (!libraryName) {
                throw new Error('getLibraryName return null');
            }
            if (!tableName) {
                throw new Error('getTableName return null');
            }
            this._localKey = `${libraryName}-${tableName}`;
        }
        return this._localKey;
    }
    protected _ctor_(): void {
        this._defineProperty();
    }
    /**重定义本地数据的属性，以方便使用 */
    private _defineProperty() {
        if (!this.localData) return;
        let localData,
            localKey = this.localKey,
            localDataStr = sys.localStorage.getItem(this.localKey);
        if (localDataStr) {
            localData = JSON.parse(localDataStr);
        } else {
            localData = {};
        }
        Object.keys(this.localData).forEach(key => {
            //这里是给默认值的
            localData[key] = typeof localData[key] != 'undefined' ? localData[key] : this.localData[key];
            Object.defineProperty(this.localData, key, {
                enumerable: true,
                configurable: true,
                set: function (value) {
                    if (localData[key] !== value) {
                        localData[key] = value;
                        sys.localStorage.setItem(localKey, JSON.stringify(localData));
                    }
                },
                get: function () {
                    return localData[key];
                }
            })
        })
    }
    /**保存本地 */
    public save() {
        sys.localStorage.setItem(this.localKey, JSON.stringify(this.localData));
    }
    /**获取类名，继承BaseData的一定要设置 @ccclass 的类名 */
    private getClassName(): string { return (<any>this).__proto__.__classname__; }
    /**
     * 库名
     */
    private getLibraryName(): string { return 'frame'; }
    /**
     * 表名
     */
    private getTableName(): string { return this.getClassName(); }
    /**
     * 清理本地数据
     */
    public clearLocalData() {
        sys.localStorage.removeItem(this.localKey);
    }
}