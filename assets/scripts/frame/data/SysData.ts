import { SysEvent } from "../event/SysEvent";
import { LangType } from "../language/LangMgr";
import { BaseData } from "./BaseData";
import { _decorator } from 'cc';
const { ccclass } = _decorator;
/**
 * 系统设置数据
 */
ccclass('SysData')
export class SysData extends BaseData {
    protected localData = {
        language: 'cn',
        music: 1,
        effect: 1,
        musicVolume: 1,
        effectVolume: 1,
    };
    /**语言设置 */
    public set language(value: LangType) {
        if (this.localData.language != value) {
            this.localData.language = value;
            SysEvent.emit(SysEvent.LANG_CHANGE, value);
        }
    }
    public get language(): LangType {
        return this.localData.language as any;
    }
    /**音乐开关 */
    public set music(value: boolean) {
        this.localData.music = value ? 1 : 0;
    }
    public get music(): boolean {
        return this.localData.music == 1;
    }
    /**音效开关 */
    public set effect(value: boolean) {
        this.localData.effect = value ? 1 : 0;
    }
    public get effect(): boolean {
        return this.localData.effect == 1;
    }
    /**音乐大小 */
    public set musicVolume(value: number) {
        this.localData.musicVolume = value;
    }
    public get musicVolume(): number {
        return this.localData.musicVolume;
    }
    /**音效大小 */
    public set effectVolume(value: number) {
        this.effectVolume = value;
    }
    public get effectVolume(): number {
        return this.localData.effectVolume;
    }
}