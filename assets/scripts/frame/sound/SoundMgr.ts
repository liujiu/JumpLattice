/**
 * 声音管理器
 */

import { AudioClip, AudioSource, Node, _decorator, assetManager, js } from "cc";
import { BaseData } from "../data/BaseData";
import { Log } from "../log/Log";
import { ResourceUtils } from "../utils/ResourceUtils";
const { ccclass } = _decorator;
enum playStatus {
    play = 1,//播放
    stop,//停止
    pause//暂停
}

interface playSoundIF {
    loop?: boolean,//是否重复
}

interface playSoundVIF extends playSoundIF {
    path: string,//路径 模块名:路径 默认resources模块
    isVoice?: boolean,//是否是录音
    startPay?: () => void,//开始播放
    success?: (clip: AudioSource) => void,//加载成功
    fail?: () => void,//加载失败
    playEnd?: () => void,//播放结束
}

interface playSoundIFS extends playSoundIF {
    status: playStatus,//状态
    path?: string,//路径 模块名:路径 默认resources模块
    audio: AudioSource,//音频对象
    id?: string,//音频id
}

@ccclass('SoundMgr')
export class SoundMgr extends BaseData {
    protected localData = {
        musicVolume: 1,
        effectVolume: 1,
        musicOnOff: true,
        effectOnOff: true,
    };
    //音效
    private effectAudioClips: { [key: string]: playSoundIFS } = js.createMap();
    //背景音乐
    private musicAudioClip: playSoundIFS = {
        status: playStatus.play,
        loop: false,
        path: null,
        audio: null,
    };
    private _music: AudioSource = null;

    public set musicOnOff(value: boolean) {
        this.localData.musicOnOff = value;
        if (!value) {
            this.closeMusic();
        }
    }
    public get musicOnOff(): boolean {
        return this.localData.musicOnOff;
    }
    public set musicVolume(value: number) {
        this.localData.musicVolume = value;
        this.setMusicVolume(value);
    }
    public get musicVolume(): number {
        return this.localData.musicVolume;
    }
    public set effectOnOff(value: boolean) {
        this.localData.effectOnOff = value;
        if (!value) {
            this.closeEffect();
        }
    }
    public get effectOnOff(): boolean {
        return this.localData.effectOnOff;
    }
    public set effectVolume(value: number) {
        this.localData.effectVolume = value;
        this.setEffectVolume(value);
    }
    public get effectVolume(): number {
        return this.localData.effectVolume;
    }
    /**背景音乐 */
    private get music(): AudioSource {
        if (!this._music || !this._music.isValid) {
            let node = new Node('music');
            this._music = node.addComponent(AudioSource);
            this._music.loop = true;
            this._music.playOnAwake = true;
            this._music.volume = this.localData.musicVolume;
            // director.getScene().addChild(node);
        }
        return this._music;
    }
    private effect(): AudioSource {
        let node = new Node('effect');
        let audio = node.addComponent(AudioSource);
        audio.loop = false;
        audio.playOnAwake = true;
        audio.volume = this.localData.effectVolume;
        // director.getScene().addChild(node);
        return audio;
    }
    protected onDestroy() {
        this.stopAllEffect();
        this.stopMusic();
    }
    /**
     * 播放背景音乐
     * @param option 
     */
    public playMusic(option: playSoundVIF) {
        if (!this.localData.musicOnOff) return;
        if (this.musicAudioClip.status == playStatus.play && this.musicAudioClip.path == option.path) return;
        let loop = option.loop, path = option.path;
        if (typeof loop == 'undefined') {
            loop = true;
        }
        this.musicAudioClip.status = playStatus.play;
        this.musicAudioClip.loop = loop;
        this.musicAudioClip.path = path;

        let bundleName: string, url: string;
        if (path.indexOf(':') != -1) {
            let arr: Array<string> = path.split(':');
            bundleName = arr[0];
            url = arr[1];
        } else {
            bundleName = 'resources'
            url = path;
        }
        ResourceUtils.loadBundleRes({
            bundleName: bundleName,
            path: url,
            type: AudioClip,
            success: (clip: AudioClip) => {
                option.success && option.success(this.music);
                if (this.musicAudioClip.path === option.path &&
                    this.musicAudioClip.status != playStatus.stop) {
                    option.startPay && option.startPay();
                    this.music.clip = clip;
                    this.music.play();
                    if (option.playEnd) {
                        this.music.node.on('ended', option.playEnd);
                    }
                    this.music.node.on(AudioSource.EventType.STARTED, () => {
                        option.startPay && option.startPay();
                    });
                    if (this.musicAudioClip.status == playStatus.pause) {
                        this.pauseMusic();
                    }
                }
            },
            fail: (err) => {
                Log.warn('加载音乐失败：' + path);
                option.fail && option.fail();
            },
        })
    }
    /**
     * 播放音效
     * @param option 
     * @returns id
     */
    public playEffect(option: playSoundVIF): string {
        if (!this.localData.effectOnOff) return;
        let loop = option.loop, path = option.path;
        if (typeof loop == 'undefined') {
            loop = false;
        }
        let success = (clip: AudioClip) => {
            if (this.effectAudioClips[element.id] &&
                element.status != playStatus.stop) {
                element.audio.clip = clip;
                element.audio.loop = element.loop;
                element.audio.play();
                element.audio.node.on(AudioSource.EventType.STARTED, () => {
                    option.startPay && option.startPay();
                });
                if (element.status == playStatus.pause) {
                    this.pauseEffect(path);
                }
                if (!option.loop) {
                    element.audio.node.on(AudioSource.EventType.ENDED, () => {
                        if (element) {
                            if (element.audio.node)
                                element.audio.node.destroy();
                            this.effectAudioClips[element.id] && delete this.effectAudioClips[element.id];
                        }
                        option.playEnd && option.playEnd();
                    });
                }
            }
            option.success && option.success(element.audio);
        }
        let fail = (err) => {
            Log.warn('加载音效失败：' + path);
            option.fail && option.fail();
        }
        let element = {
            status: playStatus.play,
            loop: loop,
            audio: this.effect(),
            id: Date.now().toString(),
            path: option.path,
        }
        this.effectAudioClips[element.id] = element;
        let bundleName: string, url: string = path;
        if (url.startsWith('http://') || url.startsWith('https://')) {
            //外网加载
            assetManager.loadRemote(url, function (err, clip: AudioClip) {
                if (err) {
                    fail(err);
                } else {
                    success(clip);
                }
            });
        } else {
            //内部加载
            if (path.indexOf(':') != -1) {
                let arr: Array<string> = path.split(':');
                bundleName = arr[0];
                url = arr[1];
            } else {
                bundleName = 'resources'
            }
            ResourceUtils.loadBundleRes({
                bundleName: bundleName,
                path: url,
                type: AudioClip,
                success: (clip: AudioClip) => {
                    success(clip);
                },
                fail: (err) => {
                    fail(err);
                },
            })
        }
        return element.id;
    }
    /**
     * 设置音乐大小
     * @param value 
     */
    public setMusicVolume(value: number) {
        this.music.volume = value;
    }
    /**
     * 开启音乐
     */
    public openMusic() {
        if (this.musicAudioClip.path) {
            this.playMusic({
                path: this.musicAudioClip.path,
            });
        }
    }
    /**
     * 停止音乐（背景音乐）
     */
    public stopMusic() {
        this.musicAudioClip.status = playStatus.stop;
        this.music.stop();
    }
    public closeMusic() {
        this.stopMusic();
    }
    /**
     * 暂停音乐（背景音乐）
     */
    public pauseMusic() {
        this.musicAudioClip.status = playStatus.pause;
        this.music.pause();
    }
    /**
     * 恢复音乐（背景音乐）
     */
    public resumeMusic() {
        if (!this.localData.musicOnOff) return;
        this.musicAudioClip.status = playStatus.play;
        this.music.play();
    }
    /**
     * 背景音乐是否正在播放
     */
    public isMusicPlaying(): boolean {
        return this.music.state == AudioSource.AudioState.PLAYING;
    }
    /**
     * 设置音效大小
     * @param value 
     */
    public setEffectVolume(value: number) {
        for (const key in this.effectAudioClips) {
            const element = this.effectAudioClips[key];
            element.audio && (element.audio.volume = value);
        }
    }
    public isPlayEffect(id: string) {
        if (this.effectAudioClips[id] &&
            this.effectAudioClips[id].status == playStatus.play) {
            return true;
        }
        return false;
    }
    /**
     * 恢复音效
     * @param id 路径
     */
    public resumeEffect(id: string) {
        if (!this.localData.effectOnOff) return;
        if (this.effectAudioClips[id] &&
            this.effectAudioClips[id].status == playStatus.play) {
            this.effectAudioClips[id].status = playStatus.pause;
            this.effectAudioClips[id].audio && this.effectAudioClips[id].audio.play();
        }
    }
    /**
     * 恢复所有音效
     */
    public resumeAllEffect() {
        if (!this.localData.effectOnOff) return;
        for (const key in this.effectAudioClips) {
            this.resumeEffect(key);
        }
    }
    /**
     * 暂停音效
     * @param id 时间戳，需要使用播放时的音效ID
     */
    public pauseEffect(id: string) {
        if (this.effectAudioClips[id] &&
            this.effectAudioClips[id].status == playStatus.play) {
            this.effectAudioClips[id].status = playStatus.pause;
            this.effectAudioClips[id].audio && this.effectAudioClips[id].audio.pause();
        }
    }
    /**
     * 暂停音效
     * @param path 路径
     */
    public pauseEffectByPath(path: string) {
        for (let id in this.effectAudioClips) {
            let element = this.effectAudioClips[id];
            if (element.path == path) {
                this.pauseEffect(element.id);
            }
        }
    }
    /**
     * 暂停所有音效
     */
    public pauseAllEffect() {
        for (const key in this.effectAudioClips) {
            this.pauseEffect(key);
        }
    }
    /**
     * 停止音效
     * @param id 时间戳，需要使用播放时的音效ID
     */
    public stopEffect(id: string) {
        if (this.effectAudioClips[id]) {
            if (this.effectAudioClips[id].audio) {
                this.effectAudioClips[id].audio.stop();
                this.effectAudioClips[id].audio.node.destroy();
            }
            delete this.effectAudioClips[id];
        }
    }
    public stopEffectByPath(path: string) {
        for (let id in this.effectAudioClips) {
            let element = this.effectAudioClips[id];
            if (element.path == path) {
                this.stopEffect(element.id);
            }
        }
    }
    /**
     * 停止所有音效
     */
    public stopAllEffect() {
        for (const key in this.effectAudioClips) {
            const element = this.effectAudioClips[key];
            if (element.audio) {
                element.audio.stop();
                element.audio.node.destroy();
            }
        }
        this.effectAudioClips = {};
    }
    public closeEffect() {
        this.stopAllEffect();
    }
}