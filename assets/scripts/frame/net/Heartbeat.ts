import { HashObject } from "../core/HashObject";
import { Log } from "../log/Log";
import { Socket, SocketStatus } from "./Socket";

//接受数据格式
type MessageFormatIF = string | ArrayBuffer;

/**
 * socket 心跳
 */
export class Heartbeat extends HashObject {
    /**心跳发送间隔时间 10s */
    public static intervalTime = 10000;
    /**心跳接受超时间隔 3s */
    public static acceptOverTime = 3000;
    /**心跳失败重试次数 */
    public static retryMaxCount = 2;
    private socket: Socket;
    //发送心跳数据
    private heartbeatDataSend: MessageFormatIF = '{"cmd":1}';
    //发送心跳时间
    private sendTime: number;
    //延时Ping
    private networkDelay: number = 0;;
    //定时器ID
    private intervalId: number;
    /**重试次数 心跳接受超时重新发送次数 总共2次 */
    private retryCount: number = 0;
    //超时定时器ID
    private overTimeId: number;
    constructor(socket: Socket, sendData: MessageFormatIF) {
        super();
        this.socket = socket;
        this.setHeartbeatData(sendData);
    }
    /**
     * 设置心跳数据
     * @param sendData 
     * @param recvData 
     */
    public setHeartbeatData(sendData: MessageFormatIF) {
        if (sendData) {
            this.heartbeatDataSend = sendData;
        }
    }
    /**
     * 获取网络延时值
     */
    public getNetworkDelay(): number {
        return this.networkDelay;
    }
    /**
     * 重置心跳，message得到心跳数据后需要重置心跳
     */
    public reset() {
        this.retryCount = 0;
        //清除超时定时器
        if (this.overTimeId) {
            clearTimeout(this.overTimeId);
            this.overTimeId = undefined;
        }
        this.networkDelay = Date.now() - this.sendTime;
    }
    /**
     * 关闭心跳
     */
    public close() {
        if (this.intervalId) {
            clearInterval(this.intervalId);
            this.intervalId = undefined;
        }
        if (this.overTimeId) {
            clearTimeout(this.overTimeId);
            this.overTimeId = undefined;
        }
    }
    //开启心跳
    public open() {
        if (this.intervalId) {
            Log.warn(`%c心跳定时器 [${this.socket.getName()}] 已存在`);
        } else {
            this.sendMsg();
            this.intervalId = setInterval(() => {
                this.sendMsg();
            }, Heartbeat.acceptOverTime);
        }
    }
    //发送心跳数据
    private sendMsg() {
        if (this.socket.getState() == SocketStatus.OPEN) {
            this.retryCount++;
            this.socket.send(this.heartbeatDataSend);
            this.sendTime = Date.now();
            if (this.overTimeId) {
                clearTimeout(this.overTimeId);
                this.overTimeId = undefined;
            }
            //连接超时即关闭socket
            this.overTimeId = setTimeout(() => {
                this.overTimeId = undefined;
                //没有重试次数的情况下断开网络
                if (this.retryCount >= Heartbeat.retryMaxCount) {
                    this.retryCount = 0;
                    this.socket.close();
                } else {
                    this.sendMsg();
                }
            }, Heartbeat.acceptOverTime);
        } else {
            this.close();
            Log.socket(`[${this.socket.getName()}] 未连接 发送心跳失败 关闭心跳定时器`);
        }
    }
}