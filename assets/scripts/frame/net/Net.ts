import { Event } from "../event/Event";
import { Heartbeat } from "./Heartbeat";
import { SocketDelegateIF, Socket } from "./Socket";

/**连接器 */
export class Net extends Event {
    /**连接成功 */
    public static SOCKET_OPEN = 'Net.SOCKET_OPEN';
    /**连接关闭 */
    public static SOCKET_CLOSE = 'Net.SOCKET_CLOSE';

    protected static _name: string = 'net';
    protected static _socket: Socket;
    protected static _heartbeat: Heartbeat;
    protected static _url: string;

    private static _isInit: boolean = false;
    /**是否静默类型,静默类型网络的会自动重连,不会触发网络失败事件,默认非静默类型 */
    private static _silent: boolean = false;
    /**是否触发显示转圈，连接超时的情况下触发 */
    private static _isLoading: boolean = false;
    /**显示loading,需要子类实现 */
    protected static showLoading() { }
    /**隐藏loading,需要子类实现 */
    protected static hideLoading() { }
    /**初始化 */
    private static init() {
        if (this._isInit) return;
        let delegate: SocketDelegateIF = {
            //连接成功
            onOpen: this.onOpen,
            //接受消息
            onMessage: this.onMessage,
            //错误
            onError: this.onError,
            //关闭成功
            onClose: this.onClose
        }
        this._socket = new Socket(this._name, this._url, delegate);
        this._heartbeat = new Heartbeat(this._socket, null);
    }
    /**
     * 获取心跳发送数据
     */
    protected static getHearbeatSendData(): any {

        return null;
    }
    /**
     * 连接成功 
     */
    protected static onOpen() {
        this._heartbeat.open();
        this.emit(this.SOCKET_OPEN);
    }
    /**
     * 接受消息 
     */
    protected static onMessage(data: string | ArrayBuffer) {
        //接受到心跳消息后需要重置心跳
        // this._heartbeat.reset();
    }
    /**
     * 发生错误 
     */
    protected static onError(err) {
        this._heartbeat.close();
        if (!this._silent) this.emit(this.SOCKET_CLOSE);

    }
    /**
     * 关闭成功 
     */
    protected static onClose() {
        this._heartbeat.close();
        if (!this._silent) this.emit(this.SOCKET_CLOSE);
    }
    /**
     * 连接网络
     * @param param.url 连接地址 
     * @param param.silent 是否静默类型
     * @param param.isLoading 是否显示转圈
     */
    public static connect(param?: {
        url: string,
        silent?: boolean,
        isLoading?: boolean,
    }) {
        this.init();
        if (!param) {
            this._socket.connect();
            return;
        }
        this.setParam(param);
        this._socket.connect();
    }
    /**
     * 设置参数
     * @param param.url 连接地址 
     * @param param.isLoading 是否显示转圈
     * @param param.silent 是否静默类型
     */
    public static setParam(param: {
        url?: string,
        silent?: boolean,
        isLoading?: boolean,
    }) {
        if (typeof param.url == 'string') this._url = param.url;
        if (typeof param.silent == 'boolean') this._silent = param.silent;
        if (typeof param.isLoading == 'boolean') this._isLoading = param.isLoading;
    }
    /**
     * 发送消息
     */
    public static send() {
        let data: any = null;
        this._socket.send(data);
    }
    /**
     * 关闭 
     */
    public static close() {
        this._heartbeat.close();
        this._socket.close();
        if (this._isLoading) this.hideLoading();
    }
    /**
     * 获取Ping 值
     * @returns ping值是根据心跳计算来的
     */
    public static getPing(): number {
        return this._heartbeat.getNetworkDelay();
    }
}