import { HashObject } from "../core/HashObject";
import { Log } from "../log/Log";

export enum SocketStatus {
    /**空闲状态 */
    IDLE = 1,
    /**连接中 */
    CONNECTING,
    /**已连接 */
    OPEN,
    /**关闭状态 */
    CLOSED,//
    /**关闭中状态 */
    CLOSING,
}
export interface SocketDelegateIF {
    //连接成功
    onOpen();
    //接受消息
    onMessage(data: string | ArrayBuffer);
    //错误
    onError(err);
    //关闭成功
    onClose(msg: string);
}
export interface SocketIF {
    //连接
    connect();
    //发送消息
    send(data: string | ArrayBuffer): boolean;
    //关闭
    close();
    //获取状态
    getState(): SocketStatus;
    //获取名字
    getName(): string;
    //设置链接地址
    setUrl(url: string);
    //获取链接地址
    getUrl(): string;
}

export class Socket extends HashObject implements SocketIF {
    //代理事件
    private _delegate: SocketDelegateIF;
    //链接地址
    private _url: string;
    //链接器名字
    private _name: string;
    //socket对象
    private _webSocket: WebSocket;
    //链接器状态
    private _state: SocketStatus = SocketStatus.IDLE;
    //数据类型
    private _binaryType: BinaryType;
    private _timeOutConnect: number = -1;
    constructor(name: string, url: string, delegate: SocketDelegateIF, binaryType: BinaryType = 'blob') {
        super();
        this._name = name;
        this._url = url;
        this._delegate = delegate;
        this._binaryType = binaryType;
    }
    /**
     * 设置URL 也可以重定向URL地址
     * @param url 
     */
    setUrl(url: string) {
        this._url = url;
    }
    /**
     * 获取链接地址
     */
    getUrl(): string {
        return this._url;
    }
    /**
     * 延时链接
     * 延时链接的目的是保证重连机制正常
     */
    private startTimeOutConnect() {
        this.stopTimeOutConnect();
        this._timeOutConnect = setTimeout(() => {
            if (this._state == SocketStatus.CONNECTING) {
                this._state = SocketStatus.CLOSED;
                this.connect();
            }
        }, 500);
    }
    /**关闭延时链接 */
    private stopTimeOutConnect() {
        if (this._timeOutConnect) {
            this._timeOutConnect = -1;
            clearTimeout(this._timeOutConnect);
        }
    }
    connect() {
        if (this._state == SocketStatus.CONNECTING) {
            Log.socket(`[${this._name}] 正在连接中 : ${this._url}`);
            return;
        }
        if (this._state == SocketStatus.OPEN) {
            Log.socket(`[${this._name}] 已连接 : ${this._url}`);
            return;
        }
        Log.socket(`[${this._name}] 正在连接:${this._url}`);
        this._state = SocketStatus.CONNECTING;
        if (this._webSocket) return;
        let ws = this._webSocket = new WebSocket(this._url);
        ws.binaryType = this._binaryType;
        ws.onopen = (ev: Event) => {
            Log.socket(`[${this._name}] 连接成功`);
            this._state = SocketStatus.OPEN;
            this._delegate.onOpen();
        };
        ws.onmessage = (ev: MessageEvent) => {
            this._delegate.onMessage(ev.data);
        };
        ws.onclose = (ev: CloseEvent) => {
            this._webSocket = undefined;
            if (this._state == SocketStatus.CLOSING) {
                Log.socket(`[${this._name}] 主动断开`);
            } else {
                Log.socket(`[${this._name}] 异常中断`);
            }
            /**连接错误移除监听 */
            ws.onopen = () => { };
            ws.onmessage = () => { };
            ws.onclose = () => { };
            ws.onerror = () => { };
            if (this._state == SocketStatus.CONNECTING) {
                this._delegate.onClose(JSON.stringify(ev));
                this.startTimeOutConnect();
            } else {
                this.stopTimeOutConnect();
                this._state = SocketStatus.CLOSED;
                this._delegate.onClose(JSON.stringify(ev));
            }
        };
        ws.onerror = (ev: Event) => {
            this._webSocket = undefined;
            Log.socket(`[${this._name}] 连接错误`);
            /**连接错误移除监听 */
            ws.onopen = () => { };
            ws.onmessage = () => { };
            ws.onclose = () => { };
            ws.onerror = () => { };
            if (this._state == SocketStatus.CONNECTING) {
                this._delegate.onError(null);
                this.startTimeOutConnect();
            } else {
                this.stopTimeOutConnect();
                this._state = SocketStatus.CLOSED;
                this._delegate.onError(null);
            }
        }
    }
    send(data: string | ArrayBuffer): boolean {
        if (this._state != SocketStatus.OPEN) {
            Log.socket(`[${this._name}] 未连接 : ${this._url}`);
            return false;
        }
        if (this._webSocket && this._webSocket.readyState == WebSocket.OPEN) {
            this._webSocket.send(data);
            return true;
        }
        return false;
    }
    close() {
        this.stopTimeOutConnect();
        if (this._state == SocketStatus.IDLE) {
            Log.socket(`[${this._name}] 空闲中 : ${this._url}`);
            return;
        }
        if (this._state == SocketStatus.CLOSED) {
            Log.socket(`[${this._name}] 已关闭 : ${this._url}`);
            return;
        }
        if (this._state == SocketStatus.CLOSING) {
            Log.socket(`[${this._name}] 关闭中 : ${this._url}`);
            return;
        }
        if (this._webSocket) {
            this._state = SocketStatus.CLOSING;
            this._webSocket.close();
        } else {
            this._state = SocketStatus.CLOSED;
        }
    }
    /**
     * 获取socket 状态
     */
    getState(): SocketStatus {
        return this._state;
    }
    /**
     * 获取socket name
     */
    getName(): string {
        return this._name;
    }
}