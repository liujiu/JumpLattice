import { BUILD, EDITOR } from 'cc/env'

/**components */
export * from './components/UIAdapNode'
export * from './components/UIButton'
export * from './components/UIImage'
export * from './components/UILabel'
export * from './components/UIComponent'
export * from './components/UIScrollTable'
export * from './components/UITableCell'

/**config */
export * from './config/FrameCfg'

/**core */
export * from './core/HashObject'
export * from './core/Singleton'
export * from './core/NodePool'

/**data */
export * from './data/BaseData'
export * from './data/SysData'

/**event */
export * from './event/BaseEvent'
export * from './event/Event'
export * from './event/SysEvent'

/**file */
export * from './file/File'

/**language */
export * from './language/LangMgr'
export * from './language/LangComponent'

/**libs */

/**log */
export * from './log/Log'

/**mvp */
export * from './mvp/BasePresenter'
export * from './mvp/BaseModel'
export * from './mvp/PresenterView'
export * from './mvp/BasePanelPresenter'

/**net */
export * from './net/Http'

export * from './sound/SoundMgr'

/**utils */
export * from './utils/CommonUtils'
export * from './utils/MathUtils'
export * from './utils/ResourceUtils'
export * from './utils/StringUtils'
export * from './utils/UIUtils'
export * from './utils/ViewActionUtils'

/**view */
export * from './view/BaseView'
export * from './view/ViewMgr'
export * from './view/BasePanel'

/**日志屏蔽办法 */
if (!BUILD && false) {
    // (function () { setInterval(function () { debugger; console.clear() }, 10, 10) })()
}