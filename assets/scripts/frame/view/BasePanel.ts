import { instantiate, isValid, Node, Prefab, UITransform, _decorator } from "cc";
import { UIComponent } from "../components/UIComponent";
import { Log } from "../log/Log";
import { ResourceUtils } from "../utils/ResourceUtils";

enum loadType {
    /**加载中 */
    loading,
    /**加载成功 */
    success,
    /**加载失败 */
    fail,
    /**删除状态 */
    destroy,
}
interface panelTaskMap {
    bundleName: string,
    className: string,
    path: string,
    type: loadType,
    node?: Node,
}

const { ccclass, requireComponent } = _decorator;
@ccclass("BasePanel")
@requireComponent(UITransform)
export class BasePanel extends UIComponent {
    /**视图地址  resource:hall/prefab bundleName和资源路径 */
    public static url: string;
    private static _panelTaskMap: panelTaskMap;
    /**获取类名，一定要设置 ccclass 才可以调用此方法 */
    public static getClassName<T extends BasePanel>(this: new () => T): string {
        let name = (<any>this).prototype.__classname__;
        if (!name) throw Error('getClassName return null');
        return name;
    }
    /**
     * 绑定在父节点
     * @param this 
     */
    public static bindFromParent(parent: Node): Promise<BasePanel> {
        return new Promise((resolve, reject) => {
            if (this._panelTaskMap && this._panelTaskMap.type != loadType.fail) return;
            let urlList = this.url.split(':');
            this._panelTaskMap = {
                className: this.getClassName(),
                bundleName: urlList.length == 2 ? urlList[0] : null,
                path: urlList.length == 1 ? urlList[0] : urlList[1],
                type: loadType.loading,
            }
            ResourceUtils.loadBundleRes({
                bundleName: this._panelTaskMap.bundleName,
                path: this._panelTaskMap.path,
                type: Prefab,
                success: (asset: Prefab) => {
                    if (!isValid(parent, true)) return;
                    let node = instantiate(asset);
                    node.parent = parent;
                    this._panelTaskMap.type = loadType.success;
                    resolve(node.getComponent(BasePanel));
                },
                fail: (err: any) => {
                    Log.error(err);
                    //销毁状态
                    if (this._panelTaskMap.type == loadType.destroy) return;
                    this._panelTaskMap.type = loadType.fail;
                    reject(err);
                }
            })
        })
    }
}