import { BlockInputEvents, Camera, Canvas, instantiate, isValid, js, Layers, Node, Prefab, UITransform, Vec3 } from "cc";
import { AdapNodeType, UIAdapNode } from "../components/UIAdapNode";
import { Singleton } from "../core/Singleton";
import { Log } from "../log/Log";
import { ResourceUtils } from "../utils/ResourceUtils";
import { ViewActionUtils } from "../utils/ViewActionUtils";
import { BaseView, ViewShowAction, ViewType } from "./BaseView";
enum loadType {
    /**加载中 */
    loading,
    /**加载成功 */
    success,
    /**加载失败 */
    fail,
    /**删除状态 */
    destroy,
}

interface viewMap {
    bundleName: string,
    className: string,
    path: string,
    type: loadType,
    visable: boolean,
    node?: Node,
    viewType: ViewType,
    param: any[],
    showFunc?: (baseNode: BaseView) => void
    loadEndFunc?: (baseNode: BaseView) => void
}

export class ViewMgr extends Singleton {
    /**根节点 */
    private _rootNodes: Array<Node> = [];
    /**任务视图view */
    private _viewTaskList: Array<Array<viewMap>> = [];
    /**当前UI显示的canvas */
    private _canvas: Node;
    public set canvas(value: Node) {
        if (this._canvas != value) {
            this.clear();
            this._canvas = value;
            this._camera = this._canvas.getComponent(Canvas).cameraComponent;
            this.initRootNode();
        }
    }
    public get canvas(): Node {
        return this._canvas;
    }
    /**UI相机 */
    private _camera: Camera;
    public get camera(): Camera {
        return this._camera;
    }
    private _blackNode: Node;
    //阻塞点击
    private _blockInputNode: Node;
    /**初始化背景节点 */
    private initBlackNode(): Promise<Node> {
        return new Promise((resolve, reject) => {
            if (!this._blockInputNode || !isValid(this._blockInputNode)) {
                this._blockInputNode = new Node();
                this._blockInputNode.layer = Layers.Enum.UI_2D;
                this._blockInputNode.addComponent(UITransform);
                this._blockInputNode.addComponent(UIAdapNode).adapType = AdapNodeType.AllScreen;
                this._blockInputNode.addComponent(BlockInputEvents);
            }

            if (!this._blackNode || !isValid(this._blackNode)) {
                ResourceUtils.loadBundleRes({
                    path: 'blackSplash',
                    type: Prefab,
                    success: (asset: Prefab) => {
                        this._blackNode = instantiate(asset);
                        resolve(this._blackNode);
                    },
                    fail: () => {
                        reject(null);
                    },
                    bundleName: ""
                })
            }
            resolve(this._blackNode);
        })
    }
    /**初始化根节点 */
    private initRootNode() {
        this._rootNodes.length = 0;
        for (let i = 0; i < ViewType.Max; i++) {
            let node = new Node(i.toString());
            this._canvas.addChild(node);
            this._rootNodes.push(node);
            this._viewTaskList[i] = [];
        }
    }
    /**移除指定类型的节点 */
    private destroyNodesByType(type: ViewType) {
        this._rootNodes[type].children.forEach((v) => {
            let baseView = v.getComponent(BaseView);
            baseView && this.destroyView(baseView.constructor)
        });
    }
    /**
     * 适配视图节点的索引
     * @param taskMap 
     */
    private adapViewNodeIndex(taskMap: viewMap) {
        let list = this._viewTaskList[taskMap.viewType];
        let nodeIndex = 0, vNode: Node;
        for (let i = 0, len = list.length; i < len; i++) {
            vNode = list[i].node;
            if (vNode && isValid(vNode)) {
                vNode.setSiblingIndex(nodeIndex++);
            }
        }
    }
    /** 自动适配黑色背景 */
    private autoApapBlackBg() {
        if (!this._blackNode) return;
        let list: Array<viewMap>, vNode: Node, view: BaseView;
        let bkgParent = this._blackNode.parent, bkgParentIdx: number;
        if (bkgParent) {
            bkgParentIdx = bkgParent.getSiblingIndex();
        }
        for (let i = ViewType.Max - 1; i >= ViewType.Page; i--) {
            list = this._viewTaskList[i];
            //如果层级在上一级，优先显示
            if (bkgParentIdx && bkgParentIdx > i) continue;
            for (let j = 0, len = list.length; j < len; j++) {
                vNode = list[j].node;
                if (!vNode || !isValid(vNode) || !vNode.active) continue;
                view = vNode.getComponent(BaseView);
                if (!view.isBlackBg) continue
                if (bkgParent != this._rootNodes[list[j].viewType])
                    this._blackNode.parent = this._rootNodes[list[j].viewType]
                this._blackNode.setSiblingIndex(j);
                view.blackBgNode = this._blackNode;
                return;
            }
        }
    }
    /**自动适配点击阻塞 */
    private autoAdapBlockInput() {
        let list: Array<viewMap>, vNode: Node, view: BaseView;
        let boknParent = this._blockInputNode.parent, boknParentIdx: number;
        if (boknParent) {
            boknParentIdx = boknParent.getSiblingIndex();
        }
        for (let i = ViewType.Max - 1; i >= ViewType.Page; i--) {
            list = this._viewTaskList[i];
            //如果层级在上一级，优先显示
            if (boknParentIdx && boknParentIdx > i) continue;
            for (let j = 0, len = list.length; j < len; j++) {
                vNode = list[j].node;
                if (!vNode || !isValid(vNode) || !vNode.active) continue;
                view = vNode.getComponent(BaseView);
                if (!view.isBlockInput) continue
                if (boknParent != this._rootNodes[list[j].viewType])
                    this._blockInputNode.parent = this._rootNodes[list[j].viewType];
                this._blockInputNode.setSiblingIndex(j);
                view.blockInputNode = this._blockInputNode;
                return;
            }
        }
    }
    /**执行任务 */
    private async runTask(taskMap: viewMap) {
        /**不在的任务 */
        if (!taskMap) return;
        await this.initBlackNode();
        if (taskMap.type == loadType.success) {
            let baseView = taskMap.node.getComponent(BaseView);
            taskMap.node.active = taskMap.visable;
            if (taskMap.visable) {
                this.adapViewNodeIndex(taskMap);
                baseView.onShow(...(taskMap.param || []));
            } else {
                baseView.onHide(...(taskMap.param || []))
            }
            delete taskMap.param;
            this.autoApapBlackBg();
            this.autoAdapBlockInput();
            this.playViewAction(baseView);
        } else if (taskMap.type == loadType.loading) {
            let classObj: any = js.getClassByName(taskMap.className),
                viewNode: Node = classObj && classObj.createView();
            if (viewNode) {
                this.addViewNode(taskMap, viewNode);
            } else {
                ResourceUtils.loadBundleRes({
                    bundleName: taskMap.bundleName,
                    path: taskMap.path,
                    type: Prefab,
                    success: (asset: Prefab) => {
                        //销毁状态
                        if (taskMap.type == loadType.destroy) return;
                        this.addViewNode(taskMap, instantiate(asset));
                    },
                    fail: (err: any) => {
                        Log.error(err);
                        //销毁状态
                        if (taskMap.type == loadType.destroy) return;

                        taskMap.type = loadType.fail;
                        delete taskMap.param;
                    }
                })
            }
        } else if (taskMap.type == loadType.destroy) {
            taskMap.node && taskMap.node.destroy();
            this.autoApapBlackBg();
            this.autoAdapBlockInput();
        }
    }
    /**
     * 添加视图节点
     * @param taskMap 任务
     * @param node 添加的节点
     */
    private addViewNode(taskMap: viewMap, node: Node) {
        taskMap.node = node;
        taskMap.type = loadType.success;
        node.setPosition(new Vec3(0, 0, 0))
        let baseView = node.getComponent(BaseView);
        //加载结束执行，一般是给初始化用的
        if (taskMap.loadEndFunc) taskMap.loadEndFunc(baseView);
        let parent = this._rootNodes[taskMap.viewType];
        node.active = taskMap.visable;
        if (taskMap.viewType == ViewType.Page) {
            this.destroyNodesByType(ViewType.Page);
            //page的情况需要把pop也全部移除掉
            this.destroyNodesByType(ViewType.Pop);
            //触发动态合图以及char模式合图的重建
            // director.emit(Director.EVENT_BEFORE_SCENE_LAUNCH);
        }
        parent.addChild(node);
        this.adapViewNodeIndex(taskMap);
        node.active && baseView.onShow(...(taskMap.param || []));
        //显示后执行，一般是同步方法用的
        if (taskMap.showFunc) taskMap.showFunc(baseView);
        delete taskMap.param;
        delete taskMap.showFunc;
        this.autoApapBlackBg();
        this.autoAdapBlockInput();
        this.playViewAction(baseView);
    }
    private playViewAction(view: BaseView): Promise<Node> {
        return new Promise((resolve, reject) => {
            if (!view.node.active) return
            if (view.viewShowAction == ViewShowAction.action1) {
                ViewActionUtils.playShowViewAction1(view.node).then((n) => {
                    resolve(n);
                })
            }
        })
    }
    /**移除一个任务 */
    private removeTask(classObj: any) {
        let list = this._viewTaskList[classObj.viewType];
        let index: number = this.getViewTaskIndex(classObj.getClassName(), classObj.viewType);
        if (index != -1) {
            list.splice(index, 1);
        }
    }
    /**获取一个视图任务的索引 -1表示没有 */
    private getViewTaskIndex(className: string, viewType: number): number {
        let list = this._viewTaskList[viewType];
        for (let i = 0, len = list.length; i < len; i++) {
            if (className == list[i].className) {
                return i;
            }
        }
        return -1;
    }
    /**
     * 是否是显示的
     * @param classObj 
     * @returns 
     */
    public viewVisable(classObj: any): boolean {
        let index: number = this.getViewTaskIndex(classObj.getClassName(), classObj.viewType);
        if (index != -1) {
            let list = this._viewTaskList[classObj.viewType];
            let taskMap: viewMap = list[index];
            return taskMap.node && taskMap.node.active && taskMap.visable
        }
        return;
    }
    /**
     * 显示视图
     * @param classObj extends BaseView 基类
     * @param param 参数值
     */
    public showView(classObj: any, ...param: any[]) {
        let urlList = classObj.url.split(':');
        let list = this._viewTaskList[classObj.viewType];
        let index: number = this.getViewTaskIndex(classObj.getClassName(), classObj.viewType);
        let taskMap: viewMap;
        if (index == -1) {
            taskMap = {
                className: classObj.getClassName(),
                viewType: classObj.viewType,
                bundleName: urlList.length == 2 ? urlList[0] : null,
                path: urlList.length == 1 ? urlList[0] : urlList[1],
                type: loadType.loading,
                visable: true,
                param: param
            }
        } else {
            taskMap = list[index];
            taskMap.visable = true;
            taskMap.param = param;
            list.splice(index, 1);
        }
        //将显示的视图放在末尾
        list.push(taskMap);
        /**重新加载 */
        if (taskMap.type == loadType.fail) taskMap.type = loadType.loading;
        this.runTask(taskMap);
        let _system = {
            then: (showFunc: (baseView: any) => void) => {
                if (isValid(taskMap.node, true) && taskMap.visable) {
                    showFunc(taskMap.node.getComponent(BaseView));
                } else {
                    taskMap.showFunc = showFunc;
                }
                return _system;
            },
            loadEnd: (loadEndFunc: (baseView: any) => void) => {
                if (isValid(taskMap.node, true)) {
                    loadEndFunc(taskMap.node.getComponent(BaseView));
                } else {
                    taskMap.loadEndFunc = loadEndFunc;
                }
                return _system;
            }
        }
        return _system;
    }
    /**
     * 隐藏视图
     * @param classObj extends BaseView 基类
     * @param param 参数值
     */
    public hideView(classObj: any, ...param: any[]) {
        let index: number = this.getViewTaskIndex(classObj.getClassName(), classObj.viewType);
        if (index != -1) {
            let list = this._viewTaskList[classObj.viewType];
            let taskMap: viewMap = list[index];
            taskMap.param = param;
            taskMap.visable = false;
            //将隐藏的视图放在首位
            list.splice(index, 1);
            list.unshift(taskMap);
            this.runTask(taskMap);
        }
    }
    /**
     * 获取视图
     * @param classObj 
     */
    public getViewNode(classObj: any): Node {
        let index: number = this.getViewTaskIndex(classObj.getClassName(), classObj.viewType);
        if (index != -1) {
            let list = this._viewTaskList[classObj.viewType];
            let taskMap: viewMap = list[index];
            if (isValid(taskMap.node)) {
                return taskMap.node;
            }
        }
        return null;
    }
    /**
     * 销毁视图
     * @param classObj extends BaseView 基类
     */
    public destroyView(classObj: any) {
        let index: number = this.getViewTaskIndex(classObj.getClassName(), classObj.viewType);
        if (index != -1) {
            let list = this._viewTaskList[classObj.viewType];
            let taskMap: viewMap = list[index];
            taskMap.type = loadType.destroy;
            this.runTask(taskMap);
            //销毁删除的视图
            list.splice(index, 1);
        }
    }
    /**清理，这里一般是场景切换的时候使用 */
    public clear() {
        /**销毁任务 */
        this._viewTaskList.forEach((list) => {
            list.forEach((v) => {
                v.type = loadType.destroy;
            })
        })
        this._viewTaskList.length = 0;
        this._rootNodes.length = 0;
        delete this._canvas;
    }
}