import { Enum, Node, _decorator } from "cc";
import { UIAdapNode } from "../components/UIAdapNode";
import { UIComponent } from "../components/UIComponent";
import { ViewMgr } from "./ViewMgr";

const { ccclass, property, requireComponent } = _decorator;
export enum ViewType {
    Page,
    /**弹框层 */
    Pop,
    /**提示层 */
    Tips,
    /**通知层*/
    Notice,
    /**软提示层*/
    SoftTips,
    /**系统tips层 */
    SysTips,
    /**引导层 */
    Guide,
    /**loading层*/
    Loading,
    /**退出游戏弹框*/
    ExitTips,
    /**debug层*/
    Debug,
    /**最大数 为常驻节点*/
    Max,
}
Enum(ViewType);

export enum ViewShowAction {
    none = 0,
    /**弹出动画 */
    action1 = 1,
}
Enum(ViewShowAction);

@ccclass('BaseView')
@requireComponent(UIAdapNode)
export class BaseView extends UIComponent {
    /**视图地址  resource:hall/prefab bundleName和资源路径 */
    public static url: string;
    /**视图类型，必须要在这里定义 */
    public static viewType: ViewType = ViewType.Pop;
    /**自定义的视图创建程序 */
    public static createView() { return null; }
    /**显示视图 */
    public static show<T extends BaseView>(this: new () => T, ...param) {
        return ViewMgr.instance().showView(this, ...param);
    }
    /**隐藏视图 */
    public static hide<T extends BaseView>(this: new () => T, ...param: any[]) {
        ViewMgr.instance().hideView(this, ...param);
    }
    /**销毁视图 */
    public static destroy<T extends BaseView>(this: new () => T) {
        ViewMgr.instance().destroyView(this);
    }
    /**执行视图方法 */
    public static then<T extends BaseView>(this: new () => T, callback: (obj: T) => void) {
        let node = ViewMgr.instance().getViewNode(this);
        if (node && node.active) {
            callback(node.getComponent(this));
        }
    }
    /**获取类名，一定要设置 ccclass 才可以调用此方法 */
    public static getClassName<T extends BaseView>(this: new () => T): string {
        let name = (<any>this).prototype.__classname__;
        if (!name) throw Error('getClassName return null');
        return name;
    }

    /**是否阻塞点击 */
    @property({
        tooltip: '是否阻塞点击'
    })
    public isBlockInput = true;
    /**是否是默认黑色的背景 */
    @property({
        tooltip: '是否是默认黑色的背景'
    })
    public isBlackBg = true;

    @property({
        type: ViewShowAction,
        tooltip: `action1 弹出动画
        `
    })
    public viewShowAction: ViewShowAction = ViewShowAction.action1;
    /**黑色背景节点 */
    public blackBgNode: Node;
    public blockInputNode: Node;

    public onShow(...param: any[]) {

    }
    public onHide(...param: any[]) {

    }
    protected onDisable() {
        if (this.blackBgNode) {
            this.blackBgNode.removeFromParent();
            delete this.blackBgNode;
        }
        if (this.blockInputNode) {
            this.blockInputNode.removeFromParent();
            delete this.blockInputNode;
        }
        super.onDisable();
    }
    /**隐藏窗口 */
    public hide() {
        this.getConstructor().hide();
    }
}
