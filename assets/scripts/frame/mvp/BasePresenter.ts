import { isValid, _decorator } from "cc";
import { Singleton } from "../core/Singleton";
import { BaseModel } from "./BaseModel";
import { BasePanelPresenter } from "./BasePanelPresenter";
import { PresenterMgr } from "./PresenterMgr";
import { PresenterView } from "./PresenterView";

interface ViewClass<T extends PresenterView> {
    new(): T;
    show();
    hide();
    destroy();
}

const { ccclass } = _decorator;

/**所有基础BasePresenter都必须要设置 @ccclass 类名，如果不设置类名，编译后会变为不可识别的脚本名 */
@ccclass('BasePresenter')
export abstract class BasePresenter extends Singleton {
    public static destroy(): void {
        //管理器中移除一个presenter
        if ((<any>this)._instance) PresenterMgr.instance().removePresenter((<any>this)._instance);
        super.destroy();
    }
    /**主视图 */
    public view: PresenterView;
    private get viewClass(): ViewClass<PresenterView> {
        return this.getViewClass();
    }
    /**model模块 */
    public model: BaseModel;

    /**子项的逻辑层 */
    private _panelPresenterList: Array<BasePanelPresenter> = [];

    private _loadCount: number = 0;

    protected _ctor_(): void {
        PresenterMgr.instance().addPresenter(this);
        let modelClass = this.getModelClass();
        if (modelClass) {
            this.model = modelClass.instance();
        }
        this.init();
        this.bindEvents();
    }
    /**必须要设置视图类，可以为空 */
    protected abstract getViewClass(): any;
    /**必须要设置model类,可以为空 */
    protected abstract getModelClass(): any;

    public getView<T extends PresenterView>(): T {
        return <any>this.view;
    }
    /**
     * 给视图推动事件，采用的是节点推送方式
     * @param key key值
     * @param param 参数
     */
    protected emitView(key: string, ...param) {
        if (isValid(this.view) && this.view.node.active) {
            this.view.node.emit(key, ...param);
        }
    }
    protected init() { }
    /**销毁 */
    protected onDestroy(): void {
        this.removeEvents();
        //移除子项
        this._panelPresenterList.forEach((v) => {
            v.onDestroy();
        })
        this._panelPresenterList.length = 0;
        if (this.viewClass) this.viewClass.destroy();
    }
    /**绑定事件 */
    protected bindEvents() { }
    /**移除绑定 */
    protected removeEvents() { }
    /**加载视图结束 */
    protected loadViewEnd() { }
    /**加载全部结束，包括绑定的panel */
    protected loadNodeAllEnd() { }
    /**视图显示完成 */
    protected viewShowEnd() { }

    /**加载节点结束 */
    private _loadNodeEnd() {
        if (--this._loadCount == 0) {
            this.loadNodeAllEnd();
        }
    }
    /**显示绑定视图 */
    public showView(): Promise<BasePresenter> {
        return new Promise((resolve, reject) => {
            if (this.viewClass) {
                this._loadCount++;
                this.viewClass.show().loadEnd((v: PresenterView) => {
                    v.presenter = this;
                    this.view = v;
                    this._panelPresenterList.forEach((v) => {
                        this._loadCount++;
                        v.parentLoadEnd()
                            .then(() => {
                                this._loadNodeEnd();
                            });
                    })
                    this._loadNodeEnd();
                    this.loadViewEnd();
                }).then(() => {
                    resolve(this);
                    this.viewShowEnd();
                });
            } else {
                reject(null);
            }
        })
    }
    /**绑定panel子项 */
    public bindPanelPresenter<T extends BasePanelPresenter>(_class_: any): T {
        let _panelPresenter = new _class_(this)
        _panelPresenter.bindFromParent(this);
        this._panelPresenterList.push(_panelPresenter);
        return _panelPresenter;
    }
    /**隐藏绑定视图 */
    public hideView() {
        if (this.viewClass) this.viewClass.hide();
    }
}