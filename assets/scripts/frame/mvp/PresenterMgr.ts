import { Singleton } from "../core/Singleton";
import { BasePresenter } from "./BasePresenter";

/**
 * presenter管理器
 */
export class PresenterMgr extends Singleton {
    private _presenterList: { [key: number]: BasePresenter } = {};
    /**
     * 添加一个presenter
     * @param presenter 
     */
    public addPresenter<T extends BasePresenter>(presenter: T) {
        this._presenterList[presenter.hashCode] = presenter;
    }
    public removePresenter<T extends BasePresenter>(presenter: T) {
        delete this._presenterList[presenter.hashCode];
    }
    /**
     * 关闭所有的Presenter
     */
    public destroyAllPresenter() {
        let keys = Object.keys(this._presenterList);
        keys.forEach((key) => {
            //得到单例类
            let classObj = (<any>this._presenterList[key]).__proto__.constructor;
            classObj.destroy();
        })
    }
}