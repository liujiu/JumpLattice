import { _decorator } from "cc";
import { HashObject } from "../core/HashObject";
import { BasePanel } from "../view/BasePanel";
import { BasePresenter } from "./BasePresenter";


const { ccclass } = _decorator;

@ccclass('BasePanelPresenter')
export abstract class BasePanelPresenter extends HashObject {
    protected panel: BasePanel;
    protected parent: BasePresenter;
    private get panelClass(): any {
        return this.getPanelClass();
    }
    /**必须要设置视图类，可以为空 */
    protected abstract getPanelClass(): any;
    public bindFromParent(parent: BasePresenter) {
        this.parent = parent;
    }
    /**销毁 */
    public onDestroy(): void {

    }
    /**父级界面加载完成 */
    public parentLoadEnd(): Promise<BasePanelPresenter> {
        return new Promise((resolve, reject) => {
            (this.panelClass.bindFromParent(this.parent.getView().node) as Promise<BasePanel>)
                .then((v) => {
                    this.panel = v;
                    this.loadPanelEnd();
                    this.init();
                    resolve(this);
                })
                .catch((err) => {
                    reject(err);
                });
        })
    }
    /**界面加载完成 */
    protected loadPanelEnd() { }
    /**初始化 绑定成功后才会调用 */
    protected init() {

    }
}