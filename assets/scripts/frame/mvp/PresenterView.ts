import { _decorator } from "cc";
import { BaseView } from "../view/BaseView";
import { BasePresenter } from "./BasePresenter";

const { ccclass, property } = _decorator;

@ccclass('PresenterView')
export class PresenterView extends BaseView {
    public presenter: BasePresenter;
    
}