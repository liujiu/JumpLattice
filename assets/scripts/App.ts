import { Component, Director, ResolutionPolicy, _decorator, director, find, macro, screen, view } from "cc";
import { ViewMgr } from "./frame";
import { SysEvent } from "./frame/event/SysEvent";


const { ccclass, property } = _decorator
/**项目入口，需要挂在首位场景 */
@ccclass('App')
export class App extends Component {
    public onLoad() {
        _app.start();
    }
}
/**app初始化 */
class _app {
    private static _app;
    public static start() {
        if (!this._app) {
            this._app = new _app();
        }
    }
    constructor() {
        this.init();
        this.bindEvents();
    }
    private init() {
        /**初始化的时候显示 */
        ViewMgr.instance().canvas = find('Canvas');
    }
    private bindEvents() {
        SysEvent.onList(this, [
            [SysEvent.SHOW_LOADING, this.showLoading],
            [SysEvent.HIDE_LOADING, this.hideLoading]
        ]);
        director.on(Director.EVENT_AFTER_SCENE_LAUNCH, this.eventAfterScendLaunch, this);
    }
    /**显示转圈 */
    private showLoading() {

    }
    /**隐藏转圈 */
    private hideLoading() {

    }
    /**运行新场景后的回调 */
    private eventAfterScendLaunch() {
        /**更新视图管理器的Canvas节点 */
        ViewMgr.instance().canvas = find('Canvas');
    }
    public destroy() {
        SysEvent.targetOff(this);
    }
}


director.once(Director.EVENT_BEFORE_SCENE_LAUNCH, () => {
    //调整适配方案
    if ((view as any)._orientation != macro.ORIENTATION_PORTRAIT) {
        var cs = screen.windowSize;
        var drs = view.getDesignResolutionSize();
        if (1 + (cs.width / cs.height - drs.width / drs.height) > 1) {
            view.setDesignResolutionSize(drs.width, drs.height, ResolutionPolicy.FIXED_HEIGHT);
        } else {
            view.setDesignResolutionSize(drs.width, drs.height, ResolutionPolicy.FIXED_WIDTH);
        }
    }
});