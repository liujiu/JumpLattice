
import { instantiate, Label, Animation, Node, _decorator, UITransform, Layers, BlockInputEvents, color, tween, v3, } from 'cc';
import { AdapNodeType, BaseView, NodePool, UIAdapNode, ViewShowAction, ViewType } from './frame';
const { ccclass, property } = _decorator;

@ccclass('MsgView')
export class MsgView extends BaseView {
    public static url: string = '';
    public static viewType: ViewType = ViewType.Tips;

    public static createView(): Node {
        let node = new Node('msgView');
        node.layer = Layers.Enum.UI_2D;
        node.addComponent(UITransform);
        node.addComponent(UIAdapNode).adapType = AdapNodeType.AllScreen;
        let msgView: MsgView = node.addComponent(MsgView);
        msgView.isBlackBg = false;
        msgView.isBlockInput = false;
        msgView.viewShowAction = ViewShowAction.none;
        return node;
    }
    private createLblMsg(): Label {
        let node = new Node('LblMsg');
        node.layer = Layers.Enum.UI_2D;
        node.addComponent(UITransform);
        let label = node.addComponent(Label);
        label.fontSize = 30;
        label.lineHeight = 32;
        label.color = color(92, 92, 92);
        return label
    }
    onShow(msg: string) {
        let lblMsg: Label = NodePool.pop<Label>('MsgView.LblMsg');
        if (!lblMsg) {
            lblMsg = this.createLblMsg();
        }
        lblMsg.string = msg;
        lblMsg.color = color(92, 92, 92, 0);
        lblMsg.node.position = v3(0, 0, 0);
        this.node.addChild(lblMsg.node);
        tween(lblMsg.node)
            .to(0.2, {
                position: v3(0, 20, 0)
            })
            .delay(0.2)
            .to(0.21, {
                position: v3(0, 40, 0)
            })
            .call(() => {
                lblMsg.node.removeFromParent();
                NodePool.push('MsgView.LblMsg', lblMsg);
                if (this.node.children.length == 0) {
                    MsgView.hide();
                }
            })
            .start();
        tween(lblMsg.color)
            .to(0.2, {
                a: 255
            }, {
                onUpdate: () => {
                    (lblMsg as any)._updateColor();
                }
            })
            .delay(0.2)
            .to(0.2, {
                a: 255
            }, {
                onUpdate: () => {
                    (lblMsg as any)._updateColor();
                }
            })
            .start();
    }
}
